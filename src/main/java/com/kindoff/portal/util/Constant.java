package com.kindoff.portal.util;

/**
 * COPYRIGHT (C) 2015 KindOff.CO LLC. All Rights Reserved. <br>
 * @author Darshana Wijegunarathna
 * @version 1.01 Feb 11, 2015
 */

public class Constant {
	
	public static final String AGE_LIST = "age_list";
	public static final String CONDITION_LIST = "condition_list";
	
	public static final String ERROR = "error";
	public static final String SUCCESS = "success";
	
	public static final String SUBSCRIPTION_CONFIRMED = "CONFIRMED";
	public static final String SUBSCRIPTION_PENDING = "PENDING";
	
	public static final String SYSTEM_EMAIL = 	"kindoff.portal@gmail.com";
	public static final long ROLE_PORTAL_USER = 1;

}

