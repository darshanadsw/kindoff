package com.kindoff.portal.util;

import java.io.Serializable;

/**
 * COPYRIGHT (C) 2015 KindOff.CO LLC. All Rights Reserved. <br>
 * @author Darshana Wijegunarathna
 * @version 1.01 Feb 12, 2015
 */

public class ServiceResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7615933419138677021L;
	
	private long id;
	private String responseType;
	private String message;
	private Object holder;
	
	public ServiceResponse(String responseType){
		this.responseType = responseType;
	}
	
	public ServiceResponse (long id, String reseponseType,String message, Object object){
		this.id = id;
		this.responseType = reseponseType;
		this.message = message;
		this.holder = object;
	}
	
	public ServiceResponse (long id, String responseType, String message) {
		this.id = id;
		this.responseType = responseType;
		this.message = message;
	}
	
	public ServiceResponse (String responseType, String message){
		this.responseType = responseType;
		this.message = message;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getResponseType() {
		return responseType;
	}

	public void setResponseType(String responseType) {
		this.responseType = responseType;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getHolder() {
		return holder;
	}

	public void setHolder(Object holder) {
		this.holder = holder;
	}

}

