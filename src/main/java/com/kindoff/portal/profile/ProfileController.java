package com.kindoff.portal.profile;

import com.kindoff.portal.common.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.Serializable;

/**
 * Created by darshana on 22/04/15.
 */
@Controller
public class ProfileController extends BaseController implements Serializable {

    private static final long serialVersionUID = -4918449129602524880L;

    @RequestMapping(value = "/editprofile", method = RequestMethod.GET)
    public String viewEditProfile(Model model) {
        return "edit_profile";
    }
}
