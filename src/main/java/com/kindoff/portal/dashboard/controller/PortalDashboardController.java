package com.kindoff.portal.dashboard.controller;

import java.io.Serializable;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * COPYRIGHT (C) 2015 KindOff.CO LLC. All Rights Reserved. <br>
 * @author Darshana Wijegunarathna
 * @version 1.01 Feb 11, 2015
 */

@Controller
public class PortalDashboardController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8480731499509044003L;
	
	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    public String viewDashboard(Model model) {
        
        return "dashboard";
    }

}

