package com.kindoff.portal.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Component;

/**
 * COPYRIGHT (C) 2015 KindOff.CO LLC. All Rights Reserved. <br>
 * @author Darshana Wijegunarathna
 * @version 1.01 Mar 1, 2015
 */

@Component("ajaxAuthenticationSuccessHandler")
public class AjaxAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {	

	public AjaxAuthenticationSuccessHandler() {
	}

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request,
			HttpServletResponse response, Authentication auth)
			throws IOException, ServletException {
		
		SavedRequest savedRequest = 
			    new HttpSessionRequestCache().getRequest(request, response);		
				    
		if ("true".equals(request.getHeader("X-Ajax-call"))) {
			response.setContentType("application/json");
			if(savedRequest != null){
				response.getWriter().print("{\"flag\":\"ok\",\"redirect\":\"" +savedRequest.getRedirectUrl() +"\"}");
			}else{
				response.getWriter().print("{\"flag\":\"ok\",\"redirect\":\"\"}");
			}            
            response.getWriter().flush();
        } else {
            super.onAuthenticationSuccess(request, response, auth);
        }

	}
}

