package com.kindoff.portal.security;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.kindoff.portal.common.controller.BaseController;
import com.kindoff.portal.common.model.PortalUser;
import com.kindoff.portal.common.model.Subscriber;
import com.kindoff.portal.common.service.PortalCommonService;
import com.kindoff.portal.util.Constant;
import com.kindoff.portal.util.Mail;
import com.kindoff.portal.util.ServiceResponse;

/**
 * COPYRIGHT (C) 2015 KindOff.CO LLC. All Rights Reserved. <br>
 * @author Darshana Wijegunarathna
 * @version 1.01 Mar 1, 2015
 */

@Controller
public class PortalLoginController extends BaseController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5745707371818750290L;
	
	@Autowired
	private PortalCommonService portalCommonService;
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(@RequestParam(value = "error", required = false) String error,
				@RequestParam(value = "logout", required = false) String logout) {

		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", "Invalid username and password!");
		}

		if (logout != null) {
			model.addObject("msg", "You've been logged out successfully.");
		}
		model.setViewName("index");

		return model;

	}
	
	//for 403 access denied page
	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public ModelAndView accesssDenied() {

		ModelAndView model = new ModelAndView();
		
		//check if user is login
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();		
		
			model.addObject("username", userDetail.getUsername());
			
		}
		
		model.setViewName("403");
		return model;

	}
	
	@RequestMapping(value = { "/", "/welcome**" }, method = RequestMethod.GET)
	public ModelAndView defaultPage() {

		ModelAndView model = new ModelAndView();		
		model.setViewName("index");
		return model;

	}
	
	@RequestMapping(value = "/register_user" , method = RequestMethod.POST)
	public @ResponseBody ServiceResponse registerPortalUser(@RequestBody PortalUser user,HttpServletRequest request){
		try{
			
			if(portalCommonService.findUserByUserName(user.getUserName()) != null){ // already registered
				SUCCESS_RESPONSE.setMessage("Looks likes this Email already exist");
			}else{
				portalCommonService.savePortalUser(user);

                // register for subscriptions.
                Mail mail = new Mail();

                mail.setMailTo(user.getEmailAddress());

                String logoPath = request.getSession().
                        getServletContext().getRealPath("/")+"favicon.ioc";

                mail.getContentMap().put("logo", logoPath);
                portalCommonService.subscribeUser(mail);

				ERROR_RESPONSE.setMessage("Great ..! Please check your email to confirm");
				return ERROR_RESPONSE;
			}

		}catch(Exception e){
			ERROR_RESPONSE.setMessage("Could not register with this user name");
			return ERROR_RESPONSE;
		}
		return SUCCESS_RESPONSE;
	}
	
	@RequestMapping(value = "/registration/confirm/{confId}", method = RequestMethod.GET)
    public String confirmRegistration(@PathVariable("confId") String confId) {        
        portalCommonService.confirmRegistration(confId);        
        return "redirect:/";
    }
	
	@RequestMapping(value = "/recover_pssword" , method = RequestMethod.POST)
	public @ResponseBody ServiceResponse recoverUserName(@RequestBody PortalUser userDeta){
		try{
			PortalUser user = portalCommonService.findUserByUserName(userDeta.getEmailAddress());
			if(user != null){
				portalCommonService.sendRecoveryLink(user.getConfId(),userDeta.getEmailAddress());
				SUCCESS_RESPONSE.setMessage("Password Recovery Link sent to your email");
			}else{
				ERROR_RESPONSE.setMessage("You are not regisiterd with this user name");
				return ERROR_RESPONSE;
			}
		}catch(Exception e){
			ERROR_RESPONSE.setMessage("Could not recover with this email");
			return ERROR_RESPONSE;
		}
		return SUCCESS_RESPONSE;
	}
	
	@RequestMapping(value = "/password/reset", method = RequestMethod.POST)
	public @ResponseBody ServiceResponse resetPassword(@RequestBody PortalUser userDeta) { 
		try {
			 portalCommonService.resetPassword(userDeta); 
			 SUCCESS_RESPONSE.setMessage("Password Reset");
		}catch ( Exception e){
			ERROR_RESPONSE.setMessage("Could not reset password");
			return ERROR_RESPONSE;
		}
              
        return SUCCESS_RESPONSE;
    }

}

