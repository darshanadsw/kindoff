package com.kindoff.portal.security;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.kindoff.portal.common.model.PortalUser;
import com.kindoff.portal.common.service.PortalCommonService;

/**
 * COPYRIGHT (C) 2015 KindOff.CO LLC. All Rights Reserved. <br>
 * @author Darshana Wijegunarathna
 * @version 1.01 Mar 12, 2015
 */

@Service
public class LoginService implements UserDetailsService {
	
	@Autowired
	private PortalCommonService portalCommonService;

	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		
		PortalUser user = portalCommonService.findUserByUserName(userName);
		List<String> roles = portalCommonService.findRoleByUserName(userName);
        List<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();

        for(String role:roles) {
            authorities.add(new SimpleGrantedAuthority(role));
        }
		
		if( user == null ) {
            throw new UsernameNotFoundException("No user found !");
        }

        return new LoginUser(user, authorities);
	}

}

