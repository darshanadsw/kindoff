package com.kindoff.portal.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import com.kindoff.portal.common.model.PortalUser;

/**
 * COPYRIGHT (C) 2015 KindOff.CO LLC. All Rights Reserved. <br>
 * @author Darshana Wijegunarathna
 * @version 1.01 Mar 12, 2015
 */

public class LoginUser extends User {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4129930613677411419L;
	
	private PortalUser portalUser;

	public PortalUser getPortalUser() {
		return portalUser;
	}

	public LoginUser(PortalUser user,Collection<? extends GrantedAuthority> authorities) {		
		super(user.getUserName(), user.getPassword(), authorities);		
		this.portalUser = user;
	}

}

