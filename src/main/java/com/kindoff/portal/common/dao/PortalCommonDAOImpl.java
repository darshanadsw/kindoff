package com.kindoff.portal.common.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.kindoff.portal.common.model.Country;
import com.kindoff.portal.common.model.CountryState;
import com.kindoff.portal.common.model.KeyValueData;
import com.kindoff.portal.common.model.PortalUser;
import com.kindoff.portal.common.model.StateCity;
import com.kindoff.portal.common.model.Subscriber;
import com.kindoff.portal.util.Constant;

/**
 * COPYRIGHT (C) 2015 KindOff.CO LLC. All Rights Reserved. <br>
 * @author Darshana Wijegunarathna
 * @version 1.01 Feb 11, 2015
 */

@Repository
public class PortalCommonDAOImpl implements PortalCommonDAO {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PortalCommonDAOImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Country> loadCountryList() {
		Session session = this.sessionFactory.getCurrentSession();
        List<Country> countryList = session.createQuery("from Country order by countryName").list();
        LOGGER.info("load country list");
        return countryList;
	}

	@Override
	public List<KeyValueData> loadKeyValueByType(String type) {
		Session session = this.sessionFactory.getCurrentSession();
		List<KeyValueData> list = session.createQuery("from KeyValueData kv where kv.type =:type order by kv.value")
				.setString("type", type).list();
		LOGGER.info("load select list " + type);
		return list;
	}

	@Override
	public List<CountryState> loadStatesByCountry(Long countryId) {	
		Session session = this.sessionFactory.getCurrentSession();
		List<CountryState> list = session.createQuery("FROM CountryState cs WHERE cs.countryId =:countryId order by cs.stateName")
						.setLong("countryId", countryId).list();
		LOGGER.info("load state list by country " + countryId);
		return list;
	}
	
	@Override
	public List<StateCity> loadCityByState(Long stateId) {		
		Session session = this.sessionFactory.getCurrentSession();
		List<StateCity> list = session.createQuery("FROM StateCity st WHERE st.stateId =:stateId order by st.cityName")
						.setLong("stateId", stateId).list();
		LOGGER.info("load state list by state " + stateId);
		return list;
	}

	@Override
	public Subscriber findSubscriberByEmail(String email) {	
		Session session = this.sessionFactory.getCurrentSession();
		LOGGER.info("Find subscriber by email : " + email);
		return (Subscriber)session.createQuery("from Subscriber sc where sc.subscriberEmail =:email")
				.setString("email", email).uniqueResult();		
	}

	@Override
	public void saveSubscriber(Subscriber subscriber) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(subscriber);
		LOGGER.info("Save subscriber");
	}

	@Override
	public void confirmSubscription(String confId) {
		Session session = this.sessionFactory.getCurrentSession();
		session.createQuery("update Subscriber set status =:status where confirmationId =:confId")
		.setParameter("status", Constant.SUBSCRIPTION_CONFIRMED)
		.setParameter("confId", confId).executeUpdate();
		LOGGER.info("Update subscriber");
	}

	@Override
	public PortalUser findUserByUserName(String userName) {
		Session session = this.sessionFactory.getCurrentSession();
		LOGGER.info("Find Portal User by user : " + userName);
		return (PortalUser)session.createQuery("from PortalUser su where su.userName =:userName")
				.setString("userName", userName).uniqueResult();	
	}

	@Override
	public void savePortalUser(PortalUser user) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(user);
		LOGGER.info("Save Portal User");
	}

	@Override
	public void confirmRegistration(String confId) {
		Session session = this.sessionFactory.getCurrentSession();
		session.createQuery("update PortalUser set active = true where conf_id =:confId")		
		.setParameter("confId", confId).executeUpdate();
		LOGGER.info("Confirm UserAccount");
	}

	@Override
	public void resetPassword(PortalUser user) {
		Session session = this.sessionFactory.getCurrentSession();
		session.createQuery("update PortalUser set password =:pass where conf_id =:confId")	
		.setParameter("pass", user.getPassword())
		.setParameter("confId", user.getConfId()).executeUpdate();
		LOGGER.info("change password");
	}

	@Override
	public List<String> findRoleByUserName(String userName) {
		LOGGER.info("Find user role by " + userName);
		Session session = this.sessionFactory.getCurrentSession();

        return (ArrayList<String>)session.createSQLQuery("select r.role from tb_users s INNER JOIN tb_role_assign rs on s.id = rs.user_id INNER JOIN    tb_user_roles r ON r.id = rs.role_id WHERE s.user_name =:userName and s.active = true").setParameter("userName", userName).list();
	}

}

