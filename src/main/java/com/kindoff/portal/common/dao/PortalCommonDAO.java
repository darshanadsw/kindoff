package com.kindoff.portal.common.dao;

import java.util.List;

import com.kindoff.portal.common.model.Country;
import com.kindoff.portal.common.model.CountryState;
import com.kindoff.portal.common.model.KeyValueData;
import com.kindoff.portal.common.model.PortalUser;
import com.kindoff.portal.common.model.StateCity;
import com.kindoff.portal.common.model.Subscriber;

/**
 * COPYRIGHT (C) 2015 KindOff.CO LLC. All Rights Reserved. <br>
 * @author Darshana Wijegunarathna
 * @version 1.01 Feb 11, 2015
 */

public interface PortalCommonDAO {
	
	List<Country> loadCountryList();
	List<KeyValueData> loadKeyValueByType(String type);
	List<CountryState> loadStatesByCountry(Long countryId);
	List<StateCity> loadCityByState(Long stateId);
	
	Subscriber findSubscriberByEmail(String email);
	void saveSubscriber(Subscriber subscriber);
	void confirmSubscription(String confId);
	
	PortalUser findUserByUserName(String userName);
	void savePortalUser(PortalUser user);
	void confirmRegistration(String confId);
	void resetPassword(PortalUser user);
	List<String> findRoleByUserName(String userName);
}

