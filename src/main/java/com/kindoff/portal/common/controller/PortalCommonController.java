package com.kindoff.portal.common.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.kindoff.portal.common.model.Country;
import com.kindoff.portal.common.model.CountryState;
import com.kindoff.portal.common.model.KeyValueData;
import com.kindoff.portal.common.model.StateCity;
import com.kindoff.portal.common.model.Subscriber;
import com.kindoff.portal.common.service.PortalCommonService;
import com.kindoff.portal.item.model.Item;
import com.kindoff.portal.util.Constant;
import com.kindoff.portal.util.Mail;
import com.kindoff.portal.util.ServiceResponse;

/**
 * COPYRIGHT (C) 2015 KindOff.CO LLC. All Rights Reserved. <br>
 * @author Darshana Wijegunarathna
 * @version 1.01 Feb 11, 2015
 */
@Controller
public class PortalCommonController extends BaseController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 599315709937444333L;
	
	@Autowired
	private PortalCommonService portalCommonService;
	
	@RequestMapping(value = "/loadCountryList", method = RequestMethod.GET)
    public @ResponseBody List<Country> loadCountryList(Model model) {
        
        return portalCommonService.loadCountryList();
    }
	
	@RequestMapping(value = "/loadKeyValue/{type}", method = RequestMethod.GET)
    public @ResponseBody List<KeyValueData> loadKeyValuesByType(@PathVariable("type") String type) {
        
        return portalCommonService.loadKeyValueByType(type);
    }
	
	@RequestMapping(value = "/loadStateList/{countryId}", method = RequestMethod.GET)
    public @ResponseBody List<CountryState> loadStateListByType(@PathVariable("countryId") Long countryId) {
        
        return portalCommonService.loadStatesByCountry(countryId);
    }
	
	@RequestMapping(value = "/loadCityList/{stateId}", method = RequestMethod.GET)
    public @ResponseBody List<StateCity> loadCityListByType(@PathVariable("stateId") Long stateId) {
        
        return portalCommonService.loadCityByState(stateId);
    }	
	
	@RequestMapping(value = "/subscribe" , method = RequestMethod.POST)
	public @ResponseBody ServiceResponse subscribeUser(@RequestBody Subscriber subscriber,HttpServletRequest request){
		try{
			Mail mail = new Mail();			
			mail.setMailTo(subscriber.getSubscriberEmail());
			
			String logoPath = request.getSession().
			 getServletContext().getRealPath("/")+"favicon.ioc";
			 
			mail.getContentMap().put("logo", logoPath);
			String result = portalCommonService.subscribeUser(mail);
			
			if(result.equals(Constant.SUBSCRIPTION_CONFIRMED)){
				SUCCESS_RESPONSE.setMessage("Looks like you already subscribe with this email address.");
			}else if(result.equals(Constant.SUBSCRIPTION_PENDING)){
				SUCCESS_RESPONSE.setMessage("Great! We've sent you an email to confirm your subscription.");
			}
			
		}catch(Exception e){
			ERROR_RESPONSE.setMessage("Could not subscribe this email");
			return ERROR_RESPONSE;
		}
		return SUCCESS_RESPONSE;
	}
	
	@RequestMapping(value = "/subscription/confirm/{confId}", method = RequestMethod.GET)
    public String loadStateListByType(@PathVariable("confId") String confId) {
        
        portalCommonService.confirmSubscription(confId);
        
        return "redirect:/";
    }
	

}

