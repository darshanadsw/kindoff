package com.kindoff.portal.common.controller;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.kindoff.portal.common.model.UserProfile;
import com.kindoff.portal.security.LoginUser;
import com.kindoff.portal.util.Constant;
import com.kindoff.portal.util.ServiceResponse;



/**
 * COPYRIGHT (C) 2015 KindOff.CO LLC. All Rights Reserved. <br>
 * @author Darshana Wijegunarathna
 * @version 1.01 Feb 12, 2015
 */

@Controller
public abstract class BaseController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5788548552568225729L;
	public static final ServiceResponse SUCCESS_RESPONSE = new ServiceResponse(Constant.SUCCESS);
	public static final ServiceResponse ERROR_RESPONSE = new ServiceResponse(Constant.ERROR);
	
	
	private HttpServletRequest request;
	
	private HttpServletResponse response;

	public UserProfile getUserProfile(){
		LoginUser user = (LoginUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		UserProfile profile = UserProfile.getUserProfile();
		profile.setUserId(user.getPortalUser().getId());
		profile.setFirstName(user.getPortalUser().getFirstName());
		profile.setLastName(user.getPortalUser().getLastName());
		
		return profile;
	}
}

