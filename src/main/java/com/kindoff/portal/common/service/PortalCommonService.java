package com.kindoff.portal.common.service;

import java.util.List;

import com.kindoff.portal.common.model.Country;
import com.kindoff.portal.common.model.CountryState;
import com.kindoff.portal.common.model.KeyValueData;
import com.kindoff.portal.common.model.PortalUser;
import com.kindoff.portal.common.model.StateCity;
import com.kindoff.portal.util.Mail;

/**
 * COPYRIGHT (C) 2015 KindOff.CO LLC. All Rights Reserved. <br>
 * @author Darshana Wijegunarathna
 * @version 1.01 Feb 11, 2015
 */

public interface PortalCommonService {

	public List<Country> loadCountryList();
	public List<KeyValueData> loadKeyValueByType(String type);
	public List<CountryState> loadStatesByCountry(Long countryId);
	public List<StateCity> loadCityByState(Long stateId);
	
	public String subscribeUser(Mail mail);
	public void confirmSubscription(String confId);
	
	public PortalUser findUserByUserName(String userName);
	public void savePortalUser(PortalUser user);
	public void confirmRegistration(String confId);
	public void sendRecoveryLink(String confId,String email);
	public void resetPassword(PortalUser user);
	public List<String> findRoleByUserName(String userName);
	
}

