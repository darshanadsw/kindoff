package com.kindoff.portal.common.service;

import java.io.StringWriter;
import java.util.*;

import javax.annotation.Resource;
import javax.mail.internet.MimeMessage;

import com.kindoff.portal.common.model.*;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.dom4j.util.UserDataDocumentFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kindoff.portal.common.dao.PortalCommonDAO;
import com.kindoff.portal.util.Constant;
import com.kindoff.portal.util.Mail;

/**
 * COPYRIGHT (C) 2015 KindOff.CO LLC. All Rights Reserved. <br>
 * @author Darshana Wijegunarathna
 * @version 1.01 Feb 11, 2015
 */

@Service
public class PortalCommonServiceImpl implements PortalCommonService {
	
	@Autowired
	private PortalCommonDAO portalCommonDao;
	@Autowired
	private VelocityEngine velocityEngine;
	@Autowired
	private JavaMailSender mailSender;
	
	@Value("${subscription.confirm.link}")
	private String subscriptionLink;
	
	@Value("${registration.confirm.link}")
	private String registrationConfLink;
	
	@Value("${password.recovery.link}")
	private String passwordRecoveryLink;
	
	@Value("${mailsender.username}")
	private String senderEmail;
	
	
	@Override
	@Transactional
	public List<Country> loadCountryList() {		
		return portalCommonDao.loadCountryList();
	}

	@Override
	@Transactional
	public List<KeyValueData> loadKeyValueByType(String type) {		
		return portalCommonDao.loadKeyValueByType(type);
	}

	@Override
	@Transactional
	public List<CountryState> loadStatesByCountry(Long countryId) {		
		return portalCommonDao.loadStatesByCountry(countryId);
	}

	@Override
	@Transactional
	public String subscribeUser(Mail mail) {
		
		String status = null;
		
		// find this email has already subscribe
		Subscriber subscriber = portalCommonDao.findSubscriberByEmail(mail.getMailTo());
		
		if (subscriber != null ) {
			if (subscriber.getStatus().equals(Constant.SUBSCRIPTION_CONFIRMED)) {
				status = Constant.SUBSCRIPTION_CONFIRMED;
			} else if (subscriber.getStatus().equals(Constant.SUBSCRIPTION_PENDING)) {
				status = Constant.SUBSCRIPTION_PENDING;
			}
		} else {
			Subscriber newSubscriber = new Subscriber();
			String confId = UUID.randomUUID().toString();
			
			newSubscriber.setActive(true);
			newSubscriber.setCreatedBy(null);
			newSubscriber.setCreatedDate(new Date());
			
			newSubscriber.setSubscriberEmail(mail.getMailTo());
			newSubscriber.setStatus(Constant.SUBSCRIPTION_PENDING);
			newSubscriber.setConfirmationId(confId);
			
			portalCommonDao.saveSubscriber(newSubscriber);
			status = Constant.SUBSCRIPTION_PENDING;
			String url = subscriptionLink.concat("/").concat(confId);
			
			mail.getContentMap().put("link", url);
			
			// send confirmation email
			sendConfirmationLink(mail);
			
		}
		
		
		return status;
	}
	
	@Override
	@Transactional
	public void confirmSubscription(String confId) {
		portalCommonDao.confirmSubscription(confId);
	}
	
	private void sendConfirmationLink(final Mail mail){
		
		final Template template = velocityEngine.getTemplate("./templates/confirmation.vm");
		  
		 MimeMessagePreparator preparator = new MimeMessagePreparator() {
				@Override
				public void prepare(MimeMessage mimeMessage) throws Exception {
					MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
					message.setFrom(senderEmail);
	                message.setTo(mail.getMailTo());
	                message.setSubject("Subscription Confirmation Link");	                
	                VelocityContext velocityContext = new VelocityContext();
	       		 	Iterator<String> iter = mail.getContentMap().keySet().iterator();
	       		 	while (iter.hasNext()) {
	       		 		String key = iter.next();
	       		 		velocityContext.put(key, mail.getContentMap().get(key));
	       		 	}
	       		 	StringWriter stringWriter = new StringWriter();		  
	       		 	template.merge(velocityContext, stringWriter);	
	                message.setText(stringWriter.toString(), true);
				}
	        };
	        
		 mailSender.send(preparator);
	}

	@Override
	@Transactional
	public List<StateCity> loadCityByState(Long stateId) {		
		return portalCommonDao.loadCityByState(stateId);
	}

	@Transactional
	public PortalUser findUserByUserName(String userName) {
		return portalCommonDao.findUserByUserName(userName);
	}

	@Transactional
	public void savePortalUser(PortalUser user) {
		
		user.setActive(false);
		user.setCreatedBy(null);
		user.setCreatedDate(new Date());

        PortalRole role = new PortalRole();
        role.setRoleId(Constant.ROLE_PORTAL_USER);
        role.setPortalUser(user);

        Set<PortalRole> roleSet = new HashSet<PortalRole>();
        roleSet.add(role);

        user.setRoles(roleSet);
		
		String confId = UUID.randomUUID().toString();
		user.setConfId(confId);


		portalCommonDao.savePortalUser(user);


		
		// send confirmation link
		Mail mail = new Mail();
		mail.setMailTo(user.getEmailAddress());
		String url = registrationConfLink.concat("/").concat(confId);		
		mail.getContentMap().put("link", url);
		
		sendRegistrationConfirmationLink(mail);
	}

	@Transactional
	public void confirmRegistration(String confId) {
		portalCommonDao.confirmRegistration(confId);
	}
	
	private void sendRegistrationConfirmationLink(final Mail mail){
		
		final Template template = velocityEngine.getTemplate("./templates/user_confirmation.vm");
		  
		 MimeMessagePreparator preparator = new MimeMessagePreparator() {
				@Override
				public void prepare(MimeMessage mimeMessage) throws Exception {
					MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
	                message.setTo(mail.getMailTo());
	                message.setSubject("Registration Confirmation Link");	                
	                VelocityContext velocityContext = new VelocityContext();
	       		 	Iterator<String> iter = mail.getContentMap().keySet().iterator();
	       		 	while (iter.hasNext()) {
	       		 		String key = iter.next();
	       		 		velocityContext.put(key, mail.getContentMap().get(key));
	       		 	}
	       		 	StringWriter stringWriter = new StringWriter();		  
	       		 	template.merge(velocityContext, stringWriter);	
	                message.setText(stringWriter.toString(), true);
				}
	        };
	        
		 mailSender.send(preparator);
	}
	
	public void sendRecoveryLink(final String confId,final String email) {
		
		final Template template = velocityEngine.getTemplate("./templates/recovery_email.vm");
		  
		 MimeMessagePreparator preparator = new MimeMessagePreparator() {
				@Override
				public void prepare(MimeMessage mimeMessage) throws Exception {
					MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
	                message.setTo(email);
	                message.setFrom(senderEmail);
	                message.setSubject("Password Recovery Link");	                
	                VelocityContext velocityContext = new VelocityContext();
	                velocityContext.put("link",passwordRecoveryLink.concat(confId).concat("&user=").concat(email));
	       		 	StringWriter stringWriter = new StringWriter();		  
	       		 	template.merge(velocityContext, stringWriter);	
	                message.setText(stringWriter.toString(), true);
				}
	        };
	        
		 mailSender.send(preparator);
	}

	@Transactional
	public void resetPassword(PortalUser user) {
		portalCommonDao.resetPassword(user);
	}
	
	@Transactional
	public List<String> findRoleByUserName(String userName) {
		return portalCommonDao.findRoleByUserName(userName);
	}

}

