package com.kindoff.portal.common.model;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * COPYRIGHT (C) 2015 KindOff.CO LLC. All Rights Reserved. <br>
 * @author Darshana Wijegunarathna
 * @version 1.01 Feb 11, 2015
 */

@Entity
@Table(name="tb_key_value_data")
public class KeyValueData extends Domain {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6390248737251242231L;
	
	
	private String key;
	private String value;
	private String type;
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

}

