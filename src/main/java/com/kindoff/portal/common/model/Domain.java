package com.kindoff.portal.common.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * COPYRIGHT (C) 2015 KindOff.CO LLC. All Rights Reserved. <br>
 * @author Darshana Wijegunarathna
 * @version 1.01 Feb 11, 2015
 */

@MappedSuperclass
public class Domain implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -565979047002849719L;
	
	@Id
	@Column(name="id")
    @GeneratedValue(strategy=GenerationType.AUTO)
	protected Long id;
	
	@Column(name="created_by")
	protected String createdBy;
	
	@Column(name="updated_by")
	protected String updatedBy;
	
	@Column(name="created_date")
	protected Date createdDate;
	
	@Column(name="updated_date")
	protected Date updatedDate;
	
	@Column(name="active")
	protected boolean isActive;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

}

