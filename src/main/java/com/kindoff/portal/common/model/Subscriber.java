package com.kindoff.portal.common.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * COPYRIGHT (C) 2015 KindOff.CO LLC. All Rights Reserved. <br>
 * @author Darshana Wijegunarathna
 * @version 1.01 Feb 18, 2015
 */

@Entity
@Table(name="tb_subscribers")
public class Subscriber extends Domain implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2325833055943669158L;
	
	@Column(name="subscribe_email")
	private String subscriberEmail;
	
	@Column(name="conf_id")
	private String confirmationId;
	
	@Column(name="`status`")
	private String status;

	public String getSubscriberEmail() {
		return subscriberEmail;
	}

	public void setSubscriberEmail(String subscriberEmail) {
		this.subscriberEmail = subscriberEmail;
	}

	public String getConfirmationId() {
		return confirmationId;
	}

	public void setConfirmationId(String confirmationId) {
		this.confirmationId = confirmationId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}

