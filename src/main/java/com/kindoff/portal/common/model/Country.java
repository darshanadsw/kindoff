package com.kindoff.portal.common.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * COPYRIGHT (C) 2015 KindOff.CO LLC. All Rights Reserved. <br>
 * @author Darshana Wijegunarathna
 * @version 1.01 Feb 11, 2015
 */

@Entity
@Table(name="tb_country")
public class Country extends Domain {	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6236350474747470869L;
	
	@Column(name="country_name")
	private String countryName;
	
	@Column(name="country_code")
	private String countryCode;
	
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	
	
}

