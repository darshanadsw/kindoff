package com.kindoff.portal.common.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * COPYRIGHT (C) 2015 KindOff.CO LLC. All Rights Reserved. <br>
 * @author Darshana Wijegunarathna
 * @versionn 1.01 Feb 16, 2015
 */
@Entity
@Table(name="tb_state")
public class CountryState extends Domain implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1199273195043736254L;
	
	@Column(name="country_id")
	private Long countryId;
	
	@Column(name="state_name")
	private String stateName;	
	
	@Column(name="state_code")
	private String stateCode;

	public Long getCountryId() {
		return countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

}

