package com.kindoff.portal.common.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * COPYRIGHT (C) 2015 KindOff.CO LLC. All Rights Reserved. <br>
 * @author Darshana Wijegunarathna
 * @version 1.01 Feb 12, 2015
 */

public class UserProfile implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7932554114743683628L;
	private static final UserProfile PROFILE = new UserProfile();
	
	private Long userId;
	private String userName;
	private List<String> userRoles = new ArrayList<String>();
	private String firstName;
	private String lastName;

	private UserProfile(){}
	
	public static UserProfile getUserProfile(){
		return PROFILE;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public List<String> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(String role) {
		this.userRoles.add(role);
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	
	
}

