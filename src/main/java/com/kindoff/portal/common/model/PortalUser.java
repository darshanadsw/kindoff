package com.kindoff.portal.common.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;

/**
 * COPYRIGHT (C) 2015 KindOff.CO LLC. All Rights Reserved. <br>
 * @author Darshana Wijegunarathna
 * @version 1.01 Mar 1, 2015
 */

@Entity
@Table(name="tb_users")
public class PortalUser extends Domain implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1682781467419101227L;
	
	@Column(name="user_name")
	private String userName;
	
	@Column(name="email_address")
	private String emailAddress;
	
	@Column(name="password")
	private String password;

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER, mappedBy = "portalUser")
	private Set<PortalRole> roles;
	
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;
	
	@Column(name="conf_id")
	private String confId;

    @Column(name="subscribe")
    private Boolean subscribe;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

    public Set<PortalRole> getRoles() {
        return roles;
    }

    public void setRoles(Set<PortalRole> roles) {
        this.roles = roles;
    }

    public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getConfId() {
		return confId;
	}

	public void setConfId(String confId) {
		this.confId = confId;
	}

    public Boolean getSubscribe() {
        return subscribe;
    }

    public void setSubscribe(Boolean subscribe) {
        this.subscribe = subscribe;
    }
}

