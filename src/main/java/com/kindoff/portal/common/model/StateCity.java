package com.kindoff.portal.common.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * COPYRIGHT (C) 2015 KindOff.CO LLC. All Rights Reserved. <br>
 * @author Darshana Wijegunarathna
 * @version 1.01 Feb 20, 2015
 */
@Entity
@Table(name="tb_city")
public class StateCity extends Domain implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1554176988787730537L;
	
	@Column(name="state_id")
	private Long stateId;
	
	@Column(name="city_name")
	private String cityName;
	
	@Column(name="city_code")
	private String cityCode;

	public Long getStateId() {
		return stateId;
	}

	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

}

