package com.kindoff.portal.common.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by darshana on 22/03/15.
 */

@Entity
@Table(name = "tb_role_assign")
public class PortalRole extends Domain implements Serializable {

    @Column(name = "role_id")
    private Long roleId;

    @ManyToOne
    @JoinColumn(name = "user_id",nullable = false)
    private PortalUser portalUser;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public PortalUser getPortalUser() {
        return portalUser;
    }

    public void setPortalUser(PortalUser portalUser) {
        this.portalUser = portalUser;
    }
}
