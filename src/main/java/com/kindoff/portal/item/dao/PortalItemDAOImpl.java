package com.kindoff.portal.item.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.kindoff.portal.common.dao.PortalCommonDAOImpl;
import com.kindoff.portal.common.model.Country;
import com.kindoff.portal.common.model.UserProfile;
import com.kindoff.portal.item.model.Item;

/**
 * COPYRIGHT (C) 2015 KindOff.CO LLC. All Rights Reserved. <br>
 * @author Darshana Wijegunarathna
 * @version 1.01 Feb 12, 2015
 */
@Repository
public class PortalItemDAOImpl implements Serializable, PortalItemDAO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7868331302459452996L;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PortalItemDAOImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void addItem(Item item, UserProfile profile) {
		Session session = this.sessionFactory.getCurrentSession();       
        LOGGER.info("Save Item to " + item.toString() );
        session.save(item);
	}

	@Override
	public void updateItem(Item item, UserProfile profile) {
		
	}

	@Override
	public void deleteItem(int id, UserProfile profile) {
		
	}

	@Override
	public Item getItemById(int id, UserProfile profile) {
		
		return null;
	}

	@Override
	public List<Item> loadItemsByCriteria(Item item, UserProfile profile) {
		
		return null;
	}

}

