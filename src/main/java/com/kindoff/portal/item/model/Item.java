package com.kindoff.portal.item.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.kindoff.portal.common.model.Domain;

/**
 * COPYRIGHT (C) 2015 KindOff.CO LLC. All Rights Reserved. <br>
 * @author Darshana Wijegunarathna
 * @version 1.01 Feb 12, 2015
 */

@Entity
@Table(name="tb_product")
public class Item extends Domain {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4507271568853282326L;
	
	@Column(name="product_name")
	private String productName;
	
	@Column(name="current_bid_price")
	private Double currentBidPrice;
	
	@Column(name="time_start")
	private Date bidStartTime;
	
	@Column(name="product_location")
	private Long productLocation;
	
	@Column(name="`condition`")
	private String condition;
	
	@Column(name="`description`")
	private String description;
	
	@Column(name="user_id")
	private Long userId;
	
	@Column(name="initial_price")
	private double initialPrice;
	
	@Column(name="age")
	private Integer age;
	
	@Column(name="sex")
	private String sex;
	
	@Column(name="delivery_method_id")
	private Long deliveryMethodId;
	
	@Column(name="payment_method_id")
	private Long paymentMethodId;
	
	@Column(name="quantity")
	private Integer quantity;
	
	@Column(name="currency")
	private String currency;
	
	@Column(name="product_location_state")
	private Long stateId;
	
	@Column(name="product_location_city")
	private Long cityId;
	
	@Column(name="product_uuid")
	private String productUUID;
	
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Double getCurrentBidPrice() {
		return currentBidPrice;
	}
	public void setCurrentBidPrice(Double currentBidPrice) {
		this.currentBidPrice = currentBidPrice;
	}
	public Date getBidStartTime() {
		return bidStartTime;
	}
	public void setBidStartTime(Date bidStartTime) {
		this.bidStartTime = bidStartTime;
	}
	
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Double getInitialPrice() {
		return initialPrice;
	}
	public void setInitialPrice(Double initialPrice) {
		this.initialPrice = initialPrice;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}	
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}	
	public Long getProductLocation() {
		return productLocation;
	}
	public void setProductLocation(Long productLocation) {
		this.productLocation = productLocation;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public long getDeliveryMethodId() {
		return deliveryMethodId;
	}
	public void setDeliveryMethodId(Long deliveryMethodId) {
		this.deliveryMethodId = deliveryMethodId;
	}
	public Long getPaymentMethodId() {
		return paymentMethodId;
	}
	public void setPaymentMethodId(Long paymentMethodId) {
		this.paymentMethodId = paymentMethodId;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public Long getStateId() {
		return stateId;
	}
	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}
	public Long getCityId() {
		return cityId;
	}
	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}	
	public String getProductUUID() {
		return productUUID;
	}
	public void setProductUUID(String productUUID) {
		this.productUUID = productUUID;
	}
	public String toString(){
		return this.id + " " + this.productName + " ";
	}
	

}

