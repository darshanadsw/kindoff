package com.kindoff.portal.item.contrller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Iterator;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.kindoff.portal.common.controller.BaseController;
import com.kindoff.portal.item.model.Item;
import com.kindoff.portal.item.model.ItemImageFile;
import com.kindoff.portal.item.service.PortalItemService;
import com.kindoff.portal.security.LoginUser;
import com.kindoff.portal.util.ServiceResponse;

/**
 * COPYRIGHT (C) 2015 KindOff.CO LLC. All Rights Reserved. <br>
 * @author Darshana Wijegunarathna
 * @version 1.01 Feb 11, 2015
 */

@Controller
public class PortalItemController extends BaseController implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5898419198326707361L;
	
	@Autowired
	private PortalItemService portalItemService;
	
	private ItemImageFile itemImageFile = null;
	
	@RequestMapping(value = "/item", method = RequestMethod.GET)
    public String viewItem(Model model) {        
        return "item";
    }
	
	@RequestMapping(value = "/add", method = RequestMethod.GET)
    public String viewAddItem(Model model) {        
        return "add";
    }
	
	@RequestMapping(value = "/addItem" , method = RequestMethod.POST)
	public @ResponseBody ServiceResponse saveItem(@RequestBody Item item){
		try{			
			item.setUserId(getUserProfile().getUserId());
			portalItemService.addItem(item, getUserProfile());
			SUCCESS_RESPONSE.setMessage("Item Added");
			SUCCESS_RESPONSE.setId(item.getId());			
		}catch(Exception e){
			ERROR_RESPONSE.setMessage("Could not Add Stuff");
			return ERROR_RESPONSE;
		}
		return SUCCESS_RESPONSE;
	}
	
	 @RequestMapping(value="/upload/{productUUID}/{imgId}", method = RequestMethod.POST)
	 public @ResponseBody ServiceResponse upload(@PathVariable("productUUID") String productUUID,@PathVariable("imgId") Integer imgId,
			 MultipartHttpServletRequest request, HttpServletResponse response) {
	 	     
	         Iterator<String> itr =  request.getFileNames();
	         MultipartFile mpf = null;	 
	      
	         while(itr.hasNext()){
	 	             
	             mpf = request.getFile(itr.next());	 
	           
	             itemImageFile = new ItemImageFile();
	             itemImageFile.setFileName(mpf.getOriginalFilename());
	             itemImageFile.setFileSize(mpf.getSize()/1024+" Kb");
	             itemImageFile.setFileType(mpf.getContentType());
	             
	             if(mpf.getSize()/1024 > 500) {
	            	 ERROR_RESPONSE.setMessage("file size bigger than 500 MB");
	            	 ERROR_RESPONSE.setHolder(itemImageFile);
	            	 return ERROR_RESPONSE;
	             }
	             
	             try {
	            	 itemImageFile.setBytes(mpf.getBytes());
	            	 
	            	 if (!mpf.isEmpty() && (mpf.getContentType().equalsIgnoreCase("image/gif") || 
	            			 mpf.getContentType().equalsIgnoreCase("image/png") || 
	            			 mpf.getContentType().equalsIgnoreCase("image/jpeg") ||
	            			 mpf.getContentType().equalsIgnoreCase("image/jpg"))) {
	            		 
	            		 
	            		 String imageType = null ;
	            		 
	            		 if(mpf.getContentType().equalsIgnoreCase("image/gif")){
	            			 imageType = "gif";
	            		 }else if(mpf.getContentType().equalsIgnoreCase("image/png")) {
	            			 imageType = "png";
	            		 }else if(mpf.getContentType().equalsIgnoreCase("image/jpg")) {
	            			 imageType = "jpg";
	            		 }else if(mpf.getContentType().equalsIgnoreCase("image/jpeg")) {
	            			 imageType = "jpg";
	            		 }else{
	            			 ERROR_RESPONSE.setMessage("Only image files in png gif or jpeg allowed");
	    	            	 ERROR_RESPONSE.setHolder(itemImageFile);
	    	            	 return ERROR_RESPONSE;
	            		 }
	            		 
	            		 File dir = new File(request.getSession().
	            				 getServletContext().getRealPath("/")+"/uploadImages/"+productUUID);
	            		 
	            		 File file = new File(request.getSession().
	            				 getServletContext().getRealPath("/")+"/uploadImages/"+productUUID+"/"+imgId+"."+imageType); 
	            		 
	            		 if(!dir.exists()){
	            			 dir.mkdirs();
	            		 }
	            		 
	            		 File fileType1 = new File(request.getSession().
	            				 getServletContext().getRealPath("/")+"/uploadImages/"+productUUID+"/"+imgId+".gif"); 
	            		 
	            		 File fileType2 = new File(request.getSession().
	            				 getServletContext().getRealPath("/")+"/uploadImages/"+productUUID+"/"+imgId+".png");
	            		 
	            		 File fileType3 = new File(request.getSession().
	            				 getServletContext().getRealPath("/")+"/uploadImages/"+productUUID+"/"+imgId+".jpg");
	            		 
	            		 if(fileType1.exists()){
	            			 fileType1.delete();
	            		 }
	            		 if(fileType2.exists()){
	            			 fileType2.delete();
	            		 }
	            		 if(fileType3.exists()){
	            			 fileType3.delete();
	            		 }
	            			 
	            		 FileOutputStream fos = new FileOutputStream(file);
	            		 BufferedOutputStream bos = new BufferedOutputStream(fos);
	            		 bos.write(mpf.getBytes());
	            		 bos.flush();
	            		 bos.close();
	            		 
	            		 itemImageFile.setFileExtension(imageType);	            		 
	            		 }  	 
	 
	            } catch (IOException e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	            }
	         }	        
	         SUCCESS_RESPONSE.setHolder(itemImageFile);
        	 
	        return SUCCESS_RESPONSE;
	    }

}

