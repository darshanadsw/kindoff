package com.kindoff.portal.item.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kindoff.portal.common.model.UserProfile;
import com.kindoff.portal.item.dao.PortalItemDAO;
import com.kindoff.portal.item.model.Item;

/**
 * COPYRIGHT (C) 2015 KindOff.CO LLC. All Rights Reserved. <br>
 * @author Darshana Wijegunarathna
 * @version 1.01 Feb 12, 2015
 */

@Service
public class PortalItemServiceImpl implements PortalItemService {
	
	@Autowired
	private PortalItemDAO portalItemDao;

	@Transactional	
	public void addItem(Item item, UserProfile profile) {
		item.setActive(true);
		item.setCreatedDate(new Date());
		portalItemDao.addItem(item, profile);
	}

	@Transactional	
	public void updateItem(Item item, UserProfile profile) {
		// TODO Auto-generated method stub
		
	}

	@Transactional	
	public void deleteItem(Integer id, UserProfile profile) {
		// TODO Auto-generated method stub
		
	}

	@Transactional	
	public Item getItemById(Integer id, UserProfile profile) {
		// TODO Auto-generated method stub
		return null;
	}

	@Transactional	
	public List<Item> loadItemsByCriteria(Item item, UserProfile profile) {
		// TODO Auto-generated method stub
		return null;
	}

}

