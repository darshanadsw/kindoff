package com.kindoff.portal.item.service;

import java.util.List;

import com.kindoff.portal.common.model.UserProfile;
import com.kindoff.portal.item.model.Item;

/**
 * COPYRIGHT (C) 2015 KindOff.CO LLC. All Rights Reserved. <br>
 * @author Darshana Wijegunarathna
 * @version 1.01 Feb 12, 2015
 */

public interface PortalItemService {

	public void addItem(Item item,UserProfile profile);
	
	public void updateItem(Item item, UserProfile profile);
	
	public void deleteItem(Integer id,UserProfile profile);
	
	public Item getItemById(Integer id,UserProfile profile);
	
	public List<Item> loadItemsByCriteria(Item item,UserProfile profile);
	
}

