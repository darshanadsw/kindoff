<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page session="true"%>
<html xml:lang="en" lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Edit Profile - Kindoff - A community of Smart Buying and Selling by Moms Worldwide</title>
    <meta name="description" content="Kindoff â€” A community of Smart Buying and Selling by Moms Worldwide">
    <meta name="keywords" content="Kindoff, baby, kids, children, e-commerce, goods, items">
    <meta name="robots" content="INDEX,FOLLOW">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <meta property="og:site_name" content="Kindoff">
    <meta property="og:description" content="Kindoff A community of Smart Buying and Selling by Moms Worldwide">
    <meta property="og:type" content="website">
    <meta property="og:url" content="http://Kindoff.co/">
    <meta property="og:image" content="http://Kindoff.co/img/opengraph/logo.png">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="stylesheet" type="text/css" href="css/base.css">
    <link rel="stylesheet" type="text/css" href="css/xcharts.min.css">
    <script src="js/jquery-2.1.1.min.js"></script>
    <script src="js/modernizr.js"></script>
    <script src="js/retina.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/xcharts.min.js"></script>
    <script src="js/d3.min.js"></script>
    <script src="js/portalutils.js"></script>
    <script src="js/subscriber.js"></script>
    <script type="text/javascript">

        $(function(){

            //Credit card block show & hide button
            $('.paymentclass').hide();
            $('.credit_card').click( function(){
                $('.paymentclass').hide();
                $('.credit_card_container').show();
            });

            $('.paypal').click( function(){
                $('.paymentclass').hide();
                $('.paypal_container').show();
            });

            $('#tradeButton-dash-1').click(function(){ window.location = 'add'});


            // load country list

            $.ajax({
                type: "GET",
                url: "loadCountryList",
                data:{},
                contentType: "application/json",
                dataType: "json",
                success: function (arten) {
                    $("#countryBox").empty();
                    $.each(arten, function () {
                        $("#countryBox").append($("<option></option>").val(this['id']).html(this['countryName']));
                    });

                }
            });

        });

    </script>
</head>
<body>
<div class="mobile-menu"> <a href="#" id="mobile-menu-close"><span class="i-close-highlight"></span> <small class="text-primary">CLOSE</small></a>
    <div class="margin-small"></div>
    <div class="list-group"> <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Diapering</a> <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>On the Go</a> <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Activities & Play</a> <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Baby Care</a> <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Nursery</a> <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Feeding</a> <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>For Mom</a> <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Health & Safety</a> </div>
    <button class="btn btn-md btn-default btn-wide"><span class="i-bubble-inverted"></span> <span class="hidden-sm">Parent's</span> Community</button>
    <div class="margin-tiny"></div>
    <button class="btn btn-md btn-default btn-wide" id="tradeButton-dash-1"><span class="i-add-inverted"></span> Trade Your Stuff</button>
    <div class="margin-medium"></div>
    Join the Community
    <ul class="list-unstyled">
        <li><a href="#">Teams</a></li>
        <li><a href="#">Forums</a></li>
        <li><a href="#">Upcoming Events</a></li>
        <li><a href="#">Affiliates</a></li>
    </ul>
    <div class="margin-medium"></div>
    Discover and Shop
    <ul class="list-unstyled">
        <li><a href="#">Gift Cards</a></li>
        <li><a href="#">Blog</a></li>
        <li><a href="#">Mobile Apps</a></li>
        <li><a href="#">Wholesale</a></li>
    </ul>
    <div class="margin-medium"></div>
    Get to Know Us
    <ul class="list-unstyled">
        <li><a href="#">About</a></li>
        <li><a href="#">Careers</a></li>
        <li><a href="#">Press</a></li>
        <li><a href="#">Developers</a></li>
    </ul>
    <div class="margin-medium"></div>
    Discover and Shop
    <ul class="list-unstyled">
        <li><a href="#">Gift Cards</a></li>
        <li><a href="#">Blog</a></li>
        <li><a href="#">Mobile Apps</a></li>
        <li><a href="#">Wholesale</a></li>
    </ul>
    <div class="margin-small"></div>
    <hr>
    <div class="margin-small"></div>
    <h4>Join Newsletter</h4>
    <div class="margin-small"></div>
    <!-- <form> -->
    <div class="input-group">
        <label for="Email" class="sr-only">Email address</label>
        <input id="subscribe-mob-box" type="email" class="form-control input-sm" placeholder="your@email.com">
    <span class="input-group-btn">
        <button id="subscribe-mob-submit" type="submit" class="btn btn-sm btn-primary">SUBSCRIBE</button>
        </span> </div>
    <!-- </form> -->
    <div class="margin-small"></div>
    <div class="icons-row"> <a href="https://twitter.com/kindoff_co" target="_blank"><span class="i-twitter"></span></a> <a href="https://www.facebook.com/kindoff.co" target="_blank"><span class="i-facebook"></span></a> <a href="http://instagram.com/kindoff.co" target="_blank"><span class="i-instagram"></span></a> </div>
</div>
<nav class="navbar navbar-default">
    <div class="container">
        <div class="row">
            <div class="col-xs-5 visible-xs header-nav">
                <ul>
                    <li>
                        <button class="btn btn-link header-mobile-menu" id="mobile-menu-open"><span class="i-menu"></span></button>
                    </li>
                    <li>
                        <button class="btn btn-link header-item-switcher"><span class="i-search"></span></button>
                        <div class="header-item-content">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search goods..." aria-label="search-global">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-transparent dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> in all categories <span class="i-arrow-down"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                                    <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                        <li><a href="#">Diapering</a></li>
                                        <li><a href="#">On the Go</a></li>
                                        <li><a href="#">Activities & Play</a></li>
                                        <li><a href="#">Baby Care</a></li>
                                        <li><a href="#">Nursery</a></li>
                                        <li><a href="#">Feeding</a></li>
                                        <li><a href="#">For Mom</a></li>
                                        <li><a href="#">Health & Safety</a></li>
                                    </ul>
                                    <button type="button" class="btn btn-default"><span class="i-search-inverted"></span></button>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-xs-2"> <a href="/KindOff"><span class="i-logo-sm hidden-xs"></span></a>
                <div class="text-center visible-xs"><a href="/KindOff"><span class="i-logo-xs"></span></a></div>
            </div>
            <div class="col-sm-6 col-lg-5 hidden-xs">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search goods..." aria-label="search-global">
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-transparent dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> in all categories <span class="i-arrow-down"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                        <ul class="dropdown-menu dropdown-menu-right" role="menu">
                            <li><a href="#">Diapering</a></li>
                            <li><a href="#">On the Go</a></li>
                            <li><a href="#">Activities & Play</a></li>
                            <li><a href="#">Baby Care</a></li>
                            <li><a href="#">Nursery</a></li>
                            <li><a href="#">Feeding</a></li>
                            <li><a href="#">For Mom</a></li>
                            <li><a href="#">Health & Safety</a></li>
                        </ul>
                        <button type="button" class="btn btn-default"><span class="i-search-inverted"></span></button>
                    </div>
                </div>
            </div>

            <div class="col-xs-5 col-sm-4 col-lg-5 header-nav">
                <ul class="pull-right">

                    <li class="header-cart">
                        <div class="top-right-cart header-item-switcher">
                            <a href="#"><span class="cart-link"><span class="count-cart">01</span></span><span class="cart-txt">Cart</span> <span class="caret"></span></a>
                        </div>
                        <div class="header-item-content">
                            <div class="carousel-cart" style="width:392px">
                                <div class="item">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="product-list-item inverted">
                                                <a href="/big" class="product-list-description">
                                                    <img src="img/square-placeholder.png" style="background-image:url(http://lorempixel.com/472/472/fashion/4);" class="img-responsive" alt="">
                                                    <span><b>Disrupt hella cray Thundercats meditation chambray slow cray Thundercats meditation chambray slow</b></span>
                                                </a>
                                                <div class="product-list-seller">
                                                    <a href="#" class="user-rating">
                                                        <span class="i-rating-inverted"></span> 6.9
                                                    </a>
                                                    <a href="#" class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/2" alt=""></a>
                                                </div>
                                                <div class="product-list-price"><s>$12</s> $7</div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="product-list-item inverted">
                                                <a href="/big" class="product-list-description">
                                                    <img src="img/square-placeholder.png" style="background-image:url(http://lorempixel.com/472/472/fashion/5);" class="img-responsive" alt="">
                                                    <span><b>Blue Bottle meggings tofu mustache. Shoreditch wolf meditation chambray slow</b></span>
                                                </a>
                                                <div class="product-list-seller">
                                                    <a href="#" class="user-rating">
                                                        <span class="i-rating-inverted"></span> 6.9
                                                    </a>
                                                    <a href="#" class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/3" alt=""></a>
                                                </div>
                                                <div class="product-list-price"><s>$12</s> $7</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="product-list-item inverted">
                                                <a href="/big" class="product-list-description">
                                                    <img src="img/square-placeholder.png" style="background-image:url(http://lorempixel.com/472/472/fashion/4);" class="img-responsive" alt="">
                                                    <span><b>Disrupt hella cray Thundercats meditation chambray slow cray Thundercats meditation chambray slow</b></span>
                                                </a>
                                                <div class="product-list-seller">
                                                    <a href="#" class="user-rating">
                                                        <span class="i-rating-inverted"></span> 6.9
                                                    </a>
                                                    <a href="#" class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/2" alt=""></a>
                                                </div>
                                                <div class="product-list-price"><s>$12</s> $7</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="submit-area clearfix">
                                <button class="btn btn-default inverted pull-right">CHECK OUT</button>
                                <div class="margin-tiny"></div>
                                3 items, grand-total: $56
                            </div>
                        </div>
                    </li>
                    <li class="header-profile">
                        <button class="btn btn-link header-item-switcher">
                            <span class="user-pic-sm"><img alt="" src="http://lorempixel.com/64/64/people/1"></span> <small class="hidden-xs">

                            <security:authorize access="isAuthenticated()">
                                <security:authentication property="principal.portalUser.firstName" />&nbsp;
                                <security:authentication property="principal.portalUser.lastName" />
                            </security:authorize>

                        </small>
                        </button>

                        <c:if test="${pageContext.request.userPrincipal.name != null}">

                            <c:url value="/j_spring_security_logout" var="logoutUrl" />
                            <form action="${logoutUrl}" method="post" id="logoutForm">
                                <input type="hidden" name="${_csrf.parameterName}"	value="${_csrf.token}" />
                            </form>

                            <script>
                                function formSubmit() {
                                    document.getElementById("logoutForm").submit();
                                }
                            </script>

                            <div class="header-item-content list-group">
                                <a class="list-group-item" href="dashboard">View Profile</a>
                                <a class="list-group-item" href="editprofile">Edit Profile</a>
                                <a class="list-group-item" href="#">Purchase History</a>
                                <a class="list-group-item" href="#">Messages <span class="badge">4</span></a>
                                <a class="list-group-item" href="#">Responses</a>
                                <a class="list-group-item" href="javascript:formSubmit()">Log Out</a>
                            </div>

                        </c:if>
                    </li>
                </ul>

                <!-- Login Modal start -->
                <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-421">
                        <div class="modal-content">
                            <div class="modal-header">

                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                                <h4 class="modal-title" id="myModalLabel">Sign In</h4>
                            </div>
                            <div class="modal-body">
                                <div class="login-regi-popup clearfix">

                                    <a class="btn facebook-btn" href="#"><i class="fa fa-facebook"> </i> Connect with Facebook</a>
                                    <a class="btn google-btn" href="#"><i class="fa fa-google-plus"> </i> Connect with Google</a>
                                    <h2 class="col-title"><span> or</span></h2>
                                    <div class="register-form clearfix">
                                        <div class="row">
                                            <div class="col-md-12"><input type="text" placeholder="User Name or email-address" class="fullwidth  icon-if-user"></div>
                                            <div class="col-md-12"><input type="password" placeholder="Password" class="fullwidth  icon-password"></div>
                                            <div class="password-status-div clearfix">
                                                <p><a href="#" data-toggle="modal" data-target="#forgotpassword" data-dismiss="modal">Lost your password?</a>.</p>
                                                <p><a href="#" data-toggle="modal" data-target="#forgotpassword" data-dismiss="modal">Forgot your username or email?</a>.</p>
                                                <p><a href="#" data-toggle="modal" data-target="#register" data-dismiss="modal">Reopen your account?</a>.</p>
                                            </div><!--end of passwors-status-div-->
                                            <div class="account-status-div clearfix">
                                                <p>Don't have an account?<a href="#" data-toggle="modal" data-target="#register" data-dismiss="modal"> Sign up here.</a>.</p>
                                            </div><!--end of passwors-status-div-->
                                            <div class="update-me clearfix">
                                                <div class="col-md-12"><span><input type="checkbox">Remenber Me</span></div>
                                                <div class="col-md-12"><span><input type="checkbox">Keep me update via e-mail.</span></div>
                                            </div>
                                        </div><!--end of row-->
                                    </div><!--end of register-form-->
                                </div><!-- end of login-regi-popup -->
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-login" data-dismiss="modal">Log in</button>
                                <button type="button" class="btn btn-clear" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Login Modal end -->

                <!-- register Modal start -->
                <div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-421">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                                <h4 class="modal-title" id="myModalLabel">Register</h4>
                            </div>
                            <div class="scrollable-pop">
                                <div class="modal-body">
                                    <div class="login-regi-popup clearfix">
                                        <a class="btn facebook-btn" href="#"><i class="fa fa-facebook"> </i> Connect with Facebook</a>
                                        <a class="btn google-btn" href="#"><i class="fa fa-google-plus"> </i> Connect with Google</a>
                                        <h2 class="col-title"><span> or</span></h2>
                                        <div class="register-form clearfix">
                                            <div class="row">
                                                <div class="col-md-6"><input type="text" placeholder="First Name" class="name"></div>
                                                <div class="col-md-6"><input type="text" placeholder="Last Name" class="name"></div>
                                                <div class="col-md-12"><input type="text" placeholder="User Name" class="fullwidth  icon-if-user"></div>
                                                <div class="col-md-12"><input type="text" placeholder="Email Address" class="fullwidth  icon-email"></div>
                                                <div class="col-md-12"><input type="password" placeholder="Password" class="fullwidth  icon-password"></div>
                                                <div class="col-md-12"><input type="password" placeholder="Confrim Password" class="fullwidth  icon-password"></div>
                                                <p class="confirmation clearfix">By clicking Register, you confirm that you accept our <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a>.</p>
                                                <div class="col-md-12"><span class="checkbox"><input type="checkbox">I want to receive Kindoff-Electric, an email news letter of fresh trends and editors' picks.</span></div>

                                            </div><!--end of row-->
                                        </div><!--end of register-form-->
                                    </div><!-- end of login-regi-popup -->
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn login-link" data-dismiss="modal">Login</button>
                                    <button type="button" class="btn btn-login" data-dismiss="modal">Register</button>
                                    <button type="button" class="btn btn-clear" data-dismiss="modal">Cancel</button>
                                </div>
                            </div><!-- end of scrollable -->
                        </div>
                    </div>
                </div>
                <!-- register Modal end -->

                <!-- forgot passwrod Modal start -->
                <div class="modal fade" id="forgotpassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-421">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                                <h4 class="modal-title" id="myModalLabel">Forgot Password</h4>
                            </div>
                            <div class="modal-body">
                                <div class="login-regi-popup clearfix">
                                    <div class="register-form clearfix">
                                        <div class="row">
                                            <div class="col-md-12"><input type="text" placeholder="Email Address" class="fullwidth  icon-email"></div>
                                        </div><!--end of row-->
                                    </div><!--end of register-form-->
                                </div><!-- end of login-regi-popup -->
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-login" data-dismiss="modal">Submit</button>
                                <button type="button" class="btn btn-clear" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- forgot passwrod Modal end -->


            </div>

        </div>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col-md-9 col-md-push-3 col-lg-10 col-lg-push-2">
            <div class="row">
                <div class="col-md-12 col-lg-7"> <a href="#" class="user-pic-md pull-left gap-right"><img src="http://lorempixel.com/160/160/people/2" alt=""></a>
                    <div class="margin-tiny"></div>
                    <h3>
                        <security:authorize access="isAuthenticated()">
                            <security:authentication property="principal.portalUser.firstName" />&nbsp;
                            <security:authentication property="principal.portalUser.lastName" />
                        </security:authorize>
                    </h3>
                    <p><input type="file"></p>
                    <div class="clearfix"></div>
                    <div class="margin-small"></div>
                    <div class="row">

                        <h3 class="head-title ml">Edit Profile</h3>
                        <div class="edit-profile-div clearfix">
                            <div class="register-form clearfix">
                                <div class="row">
                                    <div class="col-md-12"><input type="text" placeholder="First Name" class="fullwidth form-control"></div>
                                    <div class="col-md-12"><input type="text" placeholder="Last Name" class="fullwidth form-control"></div>
                                    <div class="col-md-12"><input type="text" placeholder="User Name" class="fullwidth icon-if-user form-control"></div>
                                    <div class="col-md-12"><input type="email" placeholder="Email Address" class="fullwidth  icon-email form-control"></div>
                                    <div class="col-md-12"><input type="password" placeholder="Password" class="fullwidth  icon-password form-control"></div>
                                    <div class="col-md-12"><input type="password" placeholder="Confrim Password" class="fullwidth  icon-password form-control"></div>

                                    <hr class=" clearfix divider" />
                                    <div class="col-md-12 clearfix"><textarea name="about" cols="5" rows="5" class="fullwidth txtarea-height form-control" placeholder="Bio"></textarea></div>

                                    <div class="profile-txt-div clearfix">
                                        <div class="col-md-12">
                                            <div class="col-md-2"><label>Sell In</label></div>
                                            <div class="col-md-10"><select name="month" class="form-control fullwidth"><option>-- Select --</option><option>USD</option></option><option>CAD</option><option>GB</option>P<option>INR</option></select></div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="col-md-2"><label>City</label></div>
                                            <div class="col-md-10"><select name="month" class="form-control fullwidth"><option>-- City --</option></select></div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-2"><label>Country</label></div>
                                            <div class="col-md-10">
                                                <select class="form-control small" id="countryBox">
                                                    <option>-- Country --</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="col-md-12"><p><label class="gender">Gender:</label>&nbsp;&nbsp;&nbsp;<input type="radio"> Female&nbsp;&nbsp;&nbsp;<input type="radio"> Male&nbsp;&nbsp;&nbsp;<input type="radio"> Rather not say</p></div>
                                    <div class="col-md-12"><p><label>Birthday:</label>&nbsp;&nbsp;&nbsp;<select name="month" class="form-control date-input"><option>-- Month --</option></select>&nbsp;&nbsp;&nbsp;<select name="day" class="form-control date-input"><option>-- Day --</option></select></p></div>



                                </div><!--end of row-->
                            </div><!--end of register-form-->

                            <hr class="divider">

                            <div class="col-md-12 edit-profile-btm clearfix">
                                <button type="button" class="btn btn-login" data-dismiss="modal">Edit</button>
                                <button type="button" class="btn btn-clear" data-dismiss="modal">Cancel</button>
                            </div>


                        </div><!-- end of edit-profile-div -->

                        <h3 class="head-title ml">Payment Details</h3>

                        <div class="edit-profile-div clearfix">
                            <div class="register-form clearfix">
                                <div class="row">
                                    <div class="ccd-div clearfix">

                                        <div class="col-md-12 payment-list">
                                            <p><input class="credit_card" name="paymenttype" type="radio"> <img src="img/via-cards.jpg" alt=""></p>
                                        </div>

                                        <div class="paymentclass credit_card_container clearfix">
                                            <div class="col-md-4"><p><label>Name on Card:</label><br><input type="text" placeholder="Name on Card" class="fullwidth form-control"></p></div>
                                            <div class="col-md-8"><p><label>Card number</label>:<br><input type="text" placeholder="Card Number" class="fullwidth form-control"></p></div>
                                            <div class="col-md-5"><p><label>Expiration Date:</label><br><select name="month" class="form-control date-input"><option>-- Month --</option></select>&nbsp;&nbsp;&nbsp;<select name="month" class="form-control date-input"><option>-- Year --</option></select></p></div>
                                            <div class="col-md-3"><p><label>Security Code:</label><br><input type="text" placeholder="Security Code" class="fullwidth form-control"></p></div>

                                            <div class="col-md-12 save-cancel-div clearfix">
                                                <button type="button" class="btn btn-save" data-dismiss="modal">Save</button>
                                                <button type="button" class="btn btn-cancel" data-dismiss="modal">Cancel</button>
                                            </div>

                                        </div>

                                        <div class="col-md-12 payment-list">
                                            <p><input class="paypal" type="radio" name="paymenttype"> <img src="img/paypal.jpg" alt=""></p>
                                        </div>

                                        <div class="paymentclass paypal_container clearfix">
                                            <div class="col-md-4"><p><label>Paypal id:</label><br><input type="text" placeholder="Paypal id" class="fullwidth form-control"></p></div>

                                            <div class="col-md-12 save-cancel-div clearfix">
                                                <button type="button" class="btn btn-save" data-dismiss="modal">Save</button>
                                                <button type="button" class="btn btn-cancel" data-dismiss="modal">Cancel</button>
                                            </div>

                                        </div>

                                    </div><!-- ccd div -->
                                </div><!--end of row-->
                            </div><!--end of register-form-->






                        </div><!-- end of edit-profile-div -->




                    </div>
                    <div class="margin-medium"></div>

                </div>
                <div class="col-md-12 col-lg-5">
                    <div class="cut-out-frame">
                        <h3>Seller rating</h3>
                        <a href="#" class="user-rating"><span class="i-rating-inverted"></span> 6.9</a> <small class="text-muted">based on <a href="#">16 customer reviews</a></small>
                        <div class="margin-small"></div>
                        <ul class="media-list">
                            <li class="media">
                                <div class="media-left"> <a href="#" class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/3" alt=""></a> </div>
                                <div class="media-body"> <a href="#">Natalie Amber:</a> Thank you - very good and quick service! <span class="i-rating"></span> <span class="i-rating"></span> <span class="i-rating"></span> <span class="i-rating"></span> <span class="i-rating"></span> </div>
                                <div class="margin-tiny"></div>
                            </li>
                            <li class="media">
                                <div class="media-left"> <a href="#" class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/4" alt=""></a> </div>
                                <div class="media-body"> <a href="#">Anne Page:</a> Item as described. Service was excellent. Many thanks. <span class="i-rating"></span> <span class="i-rating"></span> <span class="i-rating"></span> <span class="i-rating"></span> <span class="i-rating"></span> </div>
                                <div class="margin-tiny"></div>
                            </li>
                            <li class="media">
                                <div class="media-left"> <a href="#" class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/5" alt=""></a> </div>
                                <div class="media-body"> <a href="#">Natalie Amber:</a> Thank you - very good and quick service! <span class="i-rating"></span> <span class="i-rating"></span> <span class="i-rating"></span> <span class="i-rating"></span> <span class="i-rating"></span> </div>
                                <div class="margin-tiny"></div>
                            </li>
                            <li class="media">
                                <div class="media-left"> <a href="#" class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/6" alt=""></a> </div>
                                <div class="media-body"> <a href="#">Anne Page:</a> Item as described. Service was excellent. Many thanks. <span class="i-rating"></span> <span class="i-rating"></span> <span class="i-rating"></span> <span class="i-rating"></span> <span class="i-rating"></span> </div>
                                <div class="margin-tiny"></div>
                            </li>
                            <li class="media">
                                <div class="media-left"> <a href="#" class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/7" alt=""></a> </div>
                                <div class="media-body"> <a href="#">Natalie Amber:</a> Thank you - very good and quick service! <span class="i-rating"></span> <span class="i-rating"></span> <span class="i-rating"></span> <span class="i-rating"></span> <span class="i-rating"></span> </div>
                                <div class="margin-tiny"></div>
                            </li>
                            <li class="media">
                                <div class="media-left"> <a href="#" class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/8" alt=""></a> </div>
                                <div class="media-body"> <a href="#">Anne Page:</a> Item as described. Service was excellent. Many thanks. <span class="i-rating"></span> <span class="i-rating"></span> <span class="i-rating"></span> <span class="i-rating"></span> <span class="i-rating"></span> </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="hidden-xs">
                <div class="margin-large"></div>
                <h3><a href="#">Stuff you sell on Kindoff</a></h3>
                <div class="margin-small"></div>
                <div class="row">
                    <div class="col-xs-6 col-sm-4 col-lg-3">
                        <div class="product-list-item"> <a href="item" class="product-list-description"> <img src="img/square-placeholder.png" style="background-image:url(http://lorempixel.com/472/472/fashion/4);" class="img-responsive" alt=""> <span><b>Disrupt hella cray Thundercats meditation chambray slow cray Thundercats meditation chambray slow</b></span> </a>
                            <div class="product-list-price"><s>$12</s> $7</div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-lg-3">
                        <div class="product-list-item"> <a href="item" class="product-list-description"> <img src="img/square-placeholder.png" style="background-image:url(http://lorempixel.com/472/472/fashion/5);" class="img-responsive" alt=""> <span><b>Blue Bottle meggings tofu mustache. Shoreditch wolf meditation chambray slow</b></span> </a>
                            <div class="product-list-price"><s>$12</s> $7</div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-lg-3">
                        <div class="product-list-item"> <a href="item" class="product-list-description"> <img src="img/square-placeholder.png" style="background-image:url(http://lorempixel.com/472/472/fashion/6);" class="img-responsive" alt=""> <span><b>Messenger bag paleo mustache Pinterest. Schlitz meditation chambray slow</b></span> </a>
                            <div class="product-list-price"><s>$12</s> $7</div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-lg-3 hidden-md hidden-sm">
                        <div class="product-list-item"> <a href="item" class="product-list-description"> <img src="img/square-placeholder.png" style="background-image:url(http://lorempixel.com/472/472/fashion/7);" class="img-responsive" alt=""> <span><b>Schlitz distillery meditation mumblecore. Four loko meditation chambray slow</b></span> </a>
                            <div class="product-list-price"><s>$12</s> $7</div>
                        </div>
                    </div>
                </div>
                <div class="margin-medium"></div>
                <h3><a href="#">New stuff from people you follow</a></h3>
                <div class="margin-small"></div>
                <div class="row">
                    <div class="col-xs-6 col-sm-4 col-lg-3">
                        <div class="product-list-item"> <a href="item" class="product-list-description"> <img src="img/square-placeholder.png" style="background-image:url(http://lorempixel.com/472/472/fashion/8);" class="img-responsive" alt=""> <span><b>Disrupt hella cray Thundercats meditation chambray slow cray Thundercats meditation chambray slow</b></span> </a>
                            <div class="product-list-seller"> <a href="#" class="user-rating"> <span class="i-rating-inverted"></span> 6.9 </a> <a href="#" class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/2" alt=""></a> </div>
                            <div class="product-list-price"><s>$12</s> $7</div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-lg-3">
                        <div class="product-list-item"> <a href="item" class="product-list-description"> <img src="img/square-placeholder.png" style="background-image:url(http://lorempixel.com/472/472/fashion/9);" class="img-responsive" alt=""> <span><b>Blue Bottle meggings tofu mustache. Shoreditch wolf meditation chambray slow</b></span> </a>
                            <div class="product-list-seller"> <a href="#" class="user-rating"> <span class="i-rating-inverted"></span> 6.9 </a> <a href="#" class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/3" alt=""></a> </div>
                            <div class="product-list-price"><s>$12</s> $7</div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-lg-3">
                        <div class="product-list-item"> <a href="item" class="product-list-description"> <img src="img/square-placeholder.png" style="background-image:url(http://lorempixel.com/472/472/fashion/1);" class="img-responsive" alt=""> <span><b>Messenger bag paleo mustache Pinterest. Schlitz meditation chambray slow</b></span> </a>
                            <div class="product-list-seller"> <a href="#" class="user-rating"> <span class="i-rating-inverted"></span> 6.9 </a> <a href="#" class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/4" alt=""></a> </div>
                            <div class="product-list-price"><s>$12</s> $7</div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-lg-3 hidden-md hidden-sm">
                        <div class="product-list-item"> <a href="item" class="product-list-description"> <img src="img/square-placeholder.png" style="background-image:url(http://lorempixel.com/472/472/fashion/2);" class="img-responsive" alt=""> <span><b>Schlitz distillery meditation mumblecore. Four loko meditation chambray slow</b></span> </a>
                            <div class="product-list-seller"> <a href="#" class="user-rating"> <span class="i-rating-inverted"></span> 6.9 </a> <a href="#" class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/5" alt=""></a> </div>
                            <div class="product-list-price"><s>$12</s> $7</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-md-pull-9 col-lg-2 col-lg-pull-10">
            <hr class="visible-sm">
            <div class="margin-small visible-xs visible-sm"></div>
            <div class="row">
                <div class="col-xs-6 col-md-12">
                    <div class="list-group small">
                        <h4 class="list-group-item text-thin">Dashboard</h4>
                        <a href="#" class="list-group-item">View my public profile</a> <a href="#" class="list-group-item">Messages<span class="badge">1</span></a> <a href="#" class="list-group-item">Responses<span class="badge">2</span></a> <a href="#" class="list-group-item">Edit my data</a> <a href="#" class="list-group-item"><span class="text-muted pull-right">24</span>My Favourites</a> <a href="#" class="list-group-item"><span class="text-muted pull-right">102</span>I'm Following</a> <a href="#" class="list-group-item"><span class="text-muted pull-right">214</span>My Followers</a> </div>
                </div>
                <div class="col-xs-6 col-md-12">
                    <div class="list-group small">
                        <h4 class="list-group-item text-thin">My trading</h4>
                        <a href="#" class="list-group-item"><span class="text-muted pull-right">10</span>Manage my goods</a> <a href="#" class="list-group-item"><span class="text-muted pull-right">12</span>Purchase history</a> <a href="#" class="list-group-item"><span class="text-muted pull-right">20</span>Selling history</a> <a href="#" class="list-group-item"><span class="text-muted pull-right">1</span>Desputes</a> <a href="#" class="list-group-item">Browsing history</a> <a href="#" class="list-group-item"><span class="text-muted pull-right">2</span>My payment methods</a> <a href="#" class="list-group-item"><span class="text-muted pull-right">1</span>My widthdrawal methods</a> </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="margin-large"></div>
<footer class="hidden-xs">
    <div class="margin-medium"></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-3 col-md-2"> Join the Community
                <ul class="list-unstyled">
                    <li><a href="#">Teams</a></li>
                    <li><a href="#">Forums</a></li>
                    <li><a href="#">Upcoming Events</a></li>
                    <li><a href="#">Affiliates</a></li>
                </ul>
            </div>
            <div class="col-sm-3 col-md-2"> Discover and Shop
                <ul class="list-unstyled">
                    <li><a href="#">Gift Cards</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">Mobile Apps</a></li>
                    <li><a href="#">Wholesale</a></li>
                </ul>
            </div>
            <div class="col-sm-3 col-md-2"> Get to Know Us
                <ul class="list-unstyled">
                    <li><a href="#">About</a></li>
                    <li><a href="#">Careers</a></li>
                    <li><a href="#">Press</a></li>
                    <li><a href="#">Developers</a></li>
                </ul>
            </div>
            <div class="col-sm-3 col-md-2"> Discover and Shop
                <ul class="list-unstyled">
                    <li><a href="#">Gift Cards</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">Mobile Apps</a></li>
                    <li><a href="#">Wholesale</a></li>
                </ul>
            </div>
            <div class="col-sm-5 col-md-4 col-lg-3 col-lg-offset-1">
                <div class="margin-medium visible-sm"></div>
                <h4>Join Newsletter</h4>
                <div class="margin-small"></div>
                <!-- <form> -->
                <div class="input-group">
                    <label for="Email" class="sr-only">Email address</label>
                    <input id="subscribe-des-box" type="email" class="form-control input-sm" placeholder="your@email.com">
              <span class="input-group-btn">
          <button id="subscribe-des-submit" type="submit" class="btn btn-sm btn-primary">SUBSCRIBE</button>
          </span> </div>
                <!-- </form> -->
                <div class="margin-small"></div>
                <div class="icons-row"> <a href="https://twitter.com/kindoff_co" target="_blank"><span class="i-twitter"></span></a> <a href="https://www.facebook.com/kindoff.co" target="_blank"><span class="i-facebook"></span></a> <a href="http://instagram.com/kindoff.co" target="_blank"><span class="i-instagram"></span></a> </div>
                <div class="margin-tiny"></div>
                &copy; 2015 Kindoff. Ltd </div>
        </div>
    </div>
    <div class="margin-medium"></div>
</footer>
<div class="visible-xs small text-center text-muted"> &copy; 2015 Kindoff. Ltd
    <div class="margin-medium"></div>
</div>
<script src="js/bootstrap.min.js"></script>
</body>
</html>