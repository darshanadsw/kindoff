<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page session="true"%>
<!DOCTYPE html>
<html xml:lang="en" lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Kindoff - A community of Smart Buying and Selling by Moms Worldwide</title>
    <meta name="description" content="Kindoff - A community of Smart Buying and Selling by Moms Worldwide">
    <meta name="keywords" content="Kindoff, baby, kids, children, e-commerce, goods, items">
    <meta name="robots" content="INDEX,FOLLOW">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <meta property="og:site_name" content="Kindoff">
    <meta property="og:description" content="Kindoff - A community of Smart Buying and Selling by Moms Worldwide">
    <meta property="og:type" content="website">
    <meta property="og:url" content="http://Kindoff.co/">
    <meta property="og:image" content="http://Kindoff.co/img/opengraph/logo.png">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="stylesheet" type="text/css" href="css/base.css">
    <link rel="stylesheet" href="css/font-awesome.css" type="text/css">

    <style>
        .error {
            box-shadow: 0 0 5px rgba(255, 0, 0, 1);
            padding: 3px 0px 3px 3px;
            margin: 5px 1px 3px 0px;
            border: 1px solid rgba(255, 0, 0, 1);
        }
    </style>

    <script src="js/jquery-2.1.1.min.js"></script>
    <script src="js/modernizr.js"></script>
    <script src="js/retina.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/portalutils.js"></script>
    <script src="js/subscriber.js"></script>
    <script>
        $(function() {
            $('#tradeButton-1').click(function() {
                window.location = 'add'
            });
            $('#tradeButton-2').click(function() {
                window.location = 'add'
            });

            var checkAddress = function ()
            {
                var vars = [], hash;
                var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                for(var i = 0; i < hashes.length; i++)
                {
                    hash = hashes[i].split('=');
                    vars.push(hash[0]);
                    vars[hash[0]] = hash[1];
                }
                return vars;
            };

            $("#dropdownMenu1").on('click',function(){
                $("#ajax_login_error_log").html("");
                $('#j_ajax_password').val("");
                $('#j_ajax_username').val("");
            });

            if((checkAddress('logi') == 'logi')) {
                $("#dropdownMenu1").click();
            }

            if(checkAddress()[0] == 'conf'){
                $("#conf_id").val(checkAddress()['conf']);
                $("#forgot_user_name").val(checkAddress()['user']);
                $("#reset-pass-link").click();
            }

            // user login
            $('#login-btn').on('click',function(){

                var user_pass = $('#j_ajax_password').val();
                var user_name = $('#j_ajax_username').val();


                $.ajax({
                    url: "j_spring_security_check",
                    data: { j_username: user_name , j_password: user_pass },
                    type: "POST",
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("X-Ajax-call", "true");
                        $('#login-btn').attr("disabled");
                    },
                    success: function(result) {
                        $('#login-btn').removeAttr('disabled');
                        if (result.flag == "ok") {

                            $("#ajax_login_error_log").html("");
                            location.href = result.redirect;

                            return true;
                        }else {

                            $("#ajax_login_error_log").html('Bad username/password') ;
                            return false;
                        }
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown){
                        $('#login-btn').removeAttr('disabled');
                        console.log(errorThrown);
                        $("#ajax_login_error_log").html("Bad username/password") ;
                        return false;
                    }
                });
            });

            // user registraiton
            var reg = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;

            $("#reg-btn").click(function (){

                var firstName = $("#firstName").val();
                var lastName = $("#lastName").val();
                var userName = $("#user_name").val();
                var email_add = $("#email_add").val();
                var pass_1 = $("#pass_1").val();
                var pass_2 = $("#pass_2").val();
                var subscribe = $("#send-newsleter").val()=='yes'?true:false;

                // validate
                if(firstName == '') {
                    $("#firstName").addClass("error");
                    $("#ajax_reg_error_firstname").html("First Name is mandatory") ;
                    return false;
                }

                if(lastName == '') {
                    $("#lastName").addClass("error");
                    $("#ajax_reg_error_firstname").html("Last Name is mandatory") ;
                    return false;
                }

                if(userName == '') {
                    $("#user_name").addClass("error");
                    $("#ajax_reg_error_username").html("User Name is mandatory") ;
                    return false;
                }

                if(email_add == ''  || reg.test(email_add) == false) {
                    $("#email_add").addClass("error");
                    $("#ajax_reg_error_email").html("Email is not valid") ;
                    return false;
                }

                if(pass_1 == '') {
                    $("#pass_1").addClass("error");
                    $("#ajax_reg_error_pass1").html("Password is mandatory") ;
                    return false;
                }
                if(pass_2 == '') {
                    $("#pass_2").addClass("error");
                    $("#ajax_reg_error_pass2").html("Re-Password is mandatory") ;
                    return false;
                }
                if(pass_2 != pass_1) {
                    $("#pass_2").addClass("error");
                    $("#pass_1").addClass("error");
                    $("#ajax_reg_error_pass2").html("Password are not match") ;
                    return false;
                }

                //

                $.ajax({
                    url: 'register_user',
                    type: 'POST',
                    contentType: "application/json",
                    dataType: 'json',
                    async: false,
                    cache: false,
                    processData:false,
                    beforeSend: function(xhr) {
                        $("#reg-btn").attr("disabled");
                    },
                    data: JSON.stringify(
                            {'firstName' : $("#firstName").val(),
                                'lastName': $("#lastName").val(),
                                'userName' :$("#user_name").val(),
                                'emailAddress' :$("#email_add").val(),
                                'password' :$("#pass_1").val(),
                                'subscribe' : subscribe}),
                    success: function(data) {
                        if(data.responseType == 'error') {
                            $("#cancel-btn").click();
                            alert(data.message);
                        }else if (data.responseType == 'success'){
                            $("#ajax_reg_error_firstname").html(data.message);
                        }
                        $("#reg-btn").removeAttr('disabled');
                    }
                });

            });

            $("#firstName").on('blur',function (){
                if($(this).val() != ''){
                    $("#firstName").removeClass('error');
                    $("#ajax_reg_error_firstname").html("") ;
                }
            });

            $("#lastName").on('blur',function (){
                if($(this).val() != ''){
                    $("#lastName").removeClass('error');
                    $("#ajax_reg_error_firstname").html("") ;
                }
            });

            $("#user_name").on('blur',function (){
                if($(this).val() != '' && reg.test($(this).val())){
                    $("#user_name").removeClass('error');
                    $("#ajax_reg_error_username").html("") ;
                }
            });

            $("#pass_1").on('blur',function (){
                if($(this).val() != '' && $("#pass_2").val() != '' && ($(this).val() == $("#pass_2").val())){
                    $("#pass_1").removeClass('error');
                    $("#pass_2").removeClass('error');
                    $("#ajax_reg_error_pass1").html("") ;
                    $("#ajax_reg_error_pass2").html("") ;
                }
            });

            $("#pass_2").on('blur',function (){
                if($(this).val() != '' && $("#pass_1").val() != '' && ($(this).val() == $("#pass_1").val())){
                    $("#pass_1").removeClass('error');
                    $("#pass_2").removeClass('error');
                    $("#ajax_reg_error_pass1").html("") ;
                    $("#ajax_reg_error_pass2").html("") ;
                }
            });

            // Password recovery

            $("#reset-btn").click(function (){

                var user_name = $("#email_id").val();

                if(user_name == ''  || reg.test(user_name) == false) {
                    $("#email_id").addClass("error");
                    $("#ajax_reg_error_back_email").html("Email is not valid") ;
                    return false;
                }

                $.ajax({
                    url : 'recover_pssword',
                    type : 'post',
                    contentType : "application/json",
                    dataType : 'json',
                    data : JSON.stringify({
                        'emailAddress' : $("#email_id").val()
                    }),
                    beforeSend : function (xhr) {
                        $("#reset-btn").attr("disabled");
                    },
                    success : function(data) {
                        $("#reset-btn").removeAttr("disabled");
                        $("#rest-cancel-btn").click();
                        alert(data.message);
                    }
                });

            });

            $("#email_id").on('blur',function (){
                if($(this).val() != '' && reg.test($(this).val())){
                    $("#email_id").removeClass('error');
                    $("#ajax_reg_error_back_email").html("") ;
                }
            });


            // Reset password
            $("#reset-pass-btn").click(function (){

                var pass_1 = $("#res_pass_1").val();
                var pass_2 = $("#res_pass_2").val();

                if(pass_1 == '') {
                    $("#res_pass_1").addClass("error");
                    $("#ajax_reg_error_res_pass1").html("Password is mandatory") ;
                    return false;
                }
                if(pass_2 == '') {
                    $("#res_pass_2").addClass("error");
                    $("#ajax_reg_error_res_pass2").html("Re-Password is mandatory") ;
                    return false;
                }
                if(pass_2 != pass_1) {
                    $("#res_pass_2").addClass("error");
                    $("#res_pass_1").addClass("error");
                    $("#ajax_reg_error_res_pass2").html("Password are not match") ;
                    return false;
                }

                $.ajax({
                    url : 'password/reset',
                    type : 'post',
                    contentType : "application/json",
                    dataType : 'json',
                    data : JSON.stringify({
                        'confId' : $("#conf_id").val(),
                        'password' : $("#res_pass_1").val()
                    }),
                    beforeSend: function (xhr) {
                        $("#reset-pass-btn").attr("disabled");
                    },
                    success : function(data) {
                        alert(data.message);
                        if(data.responseType == 'success'){

                            $.ajax({
                                url: "j_spring_security_check",
                                data: { j_username: $("#forgot_user_name").val() , j_password: $("#res_pass_1").val() },
                                type: "POST",
                                beforeSend: function (xhr) {
                                    xhr.setRequestHeader("X-Ajax-call", "true");
                                },
                                success: function(result) {
                                    $("#reset-pass-btn").removeAttr("disabled");
                                    if (result.flag == "ok") {
                                        $("#ajax_login_error_log").html("");
                                        $('#reset-pass-cancel-btn').click();
                                        location.href = '<%= application.getContextPath() %>';
                                        return true;
                                    }
                                }
                            });

                        }else{

                        }
                        //$(".back_btn").click();
                    }
                });

            });

            $("#res_pass_1").on('blur',function (){
                if($(this).val() != '' && $("#res_pass_2").val() != '' && ($(this).val() == $("#res_pass_2").val())){
                    $("#res_pass_1").removeClass('error');
                    $("#res_pass_2").removeClass('error');
                    $("#ajax_reg_error_res_pass1").html("") ;
                    $("#ajax_reg_error_res_pass2").html("") ;
                }
            }); // end function

            $("#res_pass_2").on('blur',function (){
                if($(this).val() != '' && $("#res_pass_1").val() != '' && ($(this).val() == $("#res_pass_1").val())){
                    $("#res_pass_1").removeClass('error');
                    $("#res_pass_2").removeClass('error');
                    $("#ajax_reg_error_res_pass1").html("") ;
                    $("#ajax_reg_error_res_pass2").html("") ;
                }
            });


        });
    </script>
</head>
<body>
<a href="#" hidden="true" id="reset-pass-link" data-toggle="modal" aria-expanded="true" data-target="#reset-password">
    <div class="mobile-menu">
        <a href="#" id="mobile-menu-close"><span class="i-close-highlight"></span> <small class="text-primary">CLOSE</small></a>
        <div class="margin-small"></div>
        <div class="list-group">
            <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Diapering</a>
            <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>On the Go</a>
            <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Activities & Play</a>
            <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Baby Care</a>
            <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Nursery</a>
            <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Feeding</a>
            <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>For Mom</a>
            <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Health & Safety</a>
        </div>
        <button class="btn btn-md btn-default btn-wide"><span class="i-bubble-inverted"></span> <span class="hidden-sm">Parent's Community</span></button>
        <div class="margin-tiny"></div>
        <button id="tradeButton-1" class="btn btn-md btn-default btn-wide"><span class="i-add-inverted"></span> Trade Your Stuff</button>
        <div class="margin-medium"></div>
        Join the Community
        <ul class="list-unstyled">
            <li><a href="#">Teams</a></li>
            <li><a href="#">Forums</a></li>
            <li><a href="#">Upcoming Events</a></li>
            <li><a href="#">Affiliates</a></li>
        </ul>
        <div class="margin-medium"></div>
        Discover and Shop
        <ul class="list-unstyled">
            <li><a href="#">Gift Cards</a></li>
            <li><a href="#">Blog</a></li>
            <li><a href="#">Mobile Apps</a></li>
            <li><a href="#">Wholesale</a></li>
        </ul>
        <div class="margin-medium"></div>
        Get to Know Us
        <ul class="list-unstyled">
            <li><a href="#">About</a></li>
            <li><a href="#">Careers</a></li>
            <li><a href="#">Press</a></li>
            <li><a href="#">Developers</a></li>
        </ul>
        <div class="margin-medium"></div>
        Discover and Shop
        <ul class="list-unstyled">
            <li><a href="#">Gift Cards</a></li>
            <li><a href="#">Blog</a></li>
            <li><a href="#">Mobile Apps</a></li>
            <li><a href="#">Wholesale</a></li>
        </ul>
        <div class="margin-small"></div>
        <hr>
        <div class="margin-small"></div>
        <h4>Join Newsletter</h4>
        <div class="margin-small"></div>

        <div class="input-group">
            <label for="Email" class="sr-only">Email address</label>
            <input id="subscribe-des-box" type="email" class="form-control input-sm" id="Email" placeholder="your@email.com">
            <span class="input-group-btn"><button id="subscribe-des-submit" type="submit" class="btn btn-sm btn-primary">SUBSCRIBE</button></span>
        </div>

        <div class="margin-small"></div>
        <div class="icons-row">
            <a href="https://twitter.com/kindoff_co" target="_blank"><span class="i-twitter"></span></a>
            <a href="https://www.facebook.com/kindoff.co" target="_blank"><span class="i-facebook"></span></a>
            <a href="http://instagram.com/kindoff.co" target="_blank"><span class="i-instagram"></span></a>
        </div>
    </div>
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="row">
                <div class="col-xs-5 visible-xs header-nav">
                    <ul>
                        <li><button class="btn btn-link header-mobile-menu" id="mobile-menu-open"><span class="i-menu"></span></button></li>
                        <li>
                            <button class="btn btn-link header-item-switcher"><span class="i-search"></span></button>
                            <div class="header-item-content">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search goods..." aria-label="search-global">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-transparent dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                            in all categories <span class="i-arrow-down"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                            <li><a href="#">Diapering</a></li>
                                            <li><a href="#">On the Go</a></li>
                                            <li><a href="#">Activities & Play</a></li>
                                            <li><a href="#">Baby Care</a></li>
                                            <li><a href="#">Nursery</a></li>
                                            <li><a href="#">Feeding</a></li>
                                            <li><a href="#">For Mom</a></li>
                                            <li><a href="#">Health & Safety</a></li>
                                        </ul>
                                        <button type="button" class="btn btn-default"><span class="i-search-inverted"></span></button>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-2">
                    <a href="/"><span class="i-logo-sm hidden-xs"></span></a>
                    <div class="text-center visible-xs"><a href="/"><span class="i-logo-xs"></span></a></div>
                </div>
                <div class="col-sm-6 col-lg-5 hidden-xs">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search goods..." aria-label="search-global">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-transparent dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                in all categories <span class="i-arrow-down"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li><a href="#">Diapering</a></li>
                                <li><a href="#">On the Go</a></li>
                                <li><a href="#">Activities & Play</a></li>
                                <li><a href="#">Baby Care</a></li>
                                <li><a href="#">Nursery</a></li>
                                <li><a href="#">Feeding</a></li>
                                <li><a href="#">For Mom</a></li>
                                <li><a href="#">Health & Safety</a></li>
                            </ul>
                            <button type="button" class="btn btn-default"><span class="i-search-inverted"></span></button>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-5 col-lg-5 pull-right text-right">

                    <div class="top-right-nav">
                        <nav class="navbar">
                            <div class="container-fluid">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>
                                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                    <ul class="nav navbar-nav navbar-right">
                                        <li class="dropdown">
                                            <div class="top-right-signin ">
                                                <a href="#" id="dropdownMenu1" data-toggle="modal" aria-expanded="true" data-target="#login">
                                                <span class="nav-link-1">
                                                <c:if test="${pageContext.request.userPrincipal.name == null}">
                                                    Sign, In
                                                </c:if>
                                                </span> </a>
                                                <a href="#"><span class="nav-link-1"><security:authorize access="isAuthenticated()">
                                                    Hello, <security:authentication property="principal.portalUser.firstName" />&nbsp;
                                                    <security:authentication property="principal.portalUser.lastName" />
                                                </security:authorize></span></a>

                                                <c:if test="${pageContext.request.userPrincipal.name != null}">

                                                    <c:url value="/j_spring_security_logout" var="logoutUrl" />
                                                    <form action="${logoutUrl}" method="post" id="logoutForm">
                                                        <input type="hidden" name="${_csrf.parameterName}"	value="${_csrf.token}" />
                                                    </form>

                                                    <script>
                                                        function formSubmit() {
                                                            document.getElementById("logoutForm").submit();
                                                        }
                                                    </script>
                                                    <a href="javascript:formSubmit()"><span class="nav-link-2">Log Out</span> <span class="caret"></span></a>

                                                </c:if>
                                            </div><!-- end of top-right-nav -->
                                        </li>
                                        <li class="header-cart">
                                            <div class="top-right-cart top header-item-switcher">
                                                <a href="#"><span class="cart-link"><span class="count-cart">01</span></span><span class="cart-txt">Cart</span> <span class="caret"></span></a>
                                            </div>
                                            <div class="header-item-content">
                                                <div class="carousel-cart" style="width:392px">
                                                    <div class="item">
                                                        <div class="row">
                                                            <div class="col-xs-6">
                                                                <div class="product-list-item inverted">
                                                                    <a href="/big" class="product-list-description">
                                                                        <img src="img/square-placeholder.png" style="background-image:url(http://lorempixel.com/472/472/fashion/4);" class="img-responsive" alt="">
                                                                        <span><b>Disrupt hella cray Thundercats meditation chambray slow cray Thundercats meditation chambray slow</b></span>
                                                                    </a>
                                                                    <div class="product-list-seller">
                                                                        <a href="#" class="user-rating">
                                                                            <span class="i-rating-inverted"></span> 6.9
                                                                        </a>
                                                                        <a href="#" class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/2" alt=""></a>
                                                                    </div>
                                                                    <div class="product-list-price"><s>$12</s> $7</div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <div class="product-list-item inverted">
                                                                    <a href="/big" class="product-list-description">
                                                                        <img src="img/square-placeholder.png" style="background-image:url(http://lorempixel.com/472/472/fashion/5);" class="img-responsive" alt="">
                                                                        <span><b>Blue Bottle meggings tofu mustache. Shoreditch wolf meditation chambray slow</b></span>
                                                                    </a>
                                                                    <div class="product-list-seller">
                                                                        <a href="#" class="user-rating">
                                                                            <span class="i-rating-inverted"></span> 6.9
                                                                        </a>
                                                                        <a href="#" class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/3" alt=""></a>
                                                                    </div>
                                                                    <div class="product-list-price"><s>$12</s> $7</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="row">
                                                            <div class="col-xs-6">
                                                                <div class="product-list-item inverted">
                                                                    <a href="/big" class="product-list-description">
                                                                        <img src="img/square-placeholder.png" style="background-image:url(http://lorempixel.com/472/472/fashion/4);" class="img-responsive" alt="">
                                                                        <span><b>Disrupt hella cray Thundercats meditation chambray slow cray Thundercats meditation chambray slow</b></span>
                                                                    </a>
                                                                    <div class="product-list-seller">
                                                                        <a href="#" class="user-rating">
                                                                            <span class="i-rating-inverted"></span> 6.9
                                                                        </a>
                                                                        <a href="#" class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/2" alt=""></a>
                                                                    </div>
                                                                    <div class="product-list-price"><s>$12</s> $7</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="submit-area clearfix">
                                                    <button class="btn btn-default inverted pull-right">CHECK OUT</button>
                                                    <div class="margin-tiny"></div>
                                                    3 items, grand-total: $56
                                                </div>
                                            </div>
                                        </li>
                                        <!--<li class="logout-btn"><a href="#">Log Out</a></li>-->
                                    </ul>
                                </div><!-- /.navbar-collapse -->
                            </div><!-- /.container-fluid -->
                        </nav>
                    </div>


                    <!-- Login Modal start -->
                    <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-421">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Sign Up</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="login-regi-popup clearfix">

                                        <a class="btn facebook-btn" href="#"><i class="fa fa-facebook"> </i> Connect with Facebook</a>
                                        <a class="btn google-btn" href="#"><i class="fa fa-google-plus"> </i> Connect with Google</a>
                                        <h2 class="col-title"><span> or</span></h2>
                                        <center><div style="font-size: 12px; color: red;" id="ajax_login_error_log"></div></center>
                                        <div class="register-form clearfix">
                                            <div class="row">
                                                <div class="col-md-12"><input id="j_ajax_username" type="text" placeholder="User Name or email-address" class="fullwidth  icon-if-user"></div>
                                                <div class="col-md-12"><input id="j_ajax_password" type="password" placeholder="Password" class="fullwidth  icon-password"></div>
                                                <div class="password-status-div clearfix">
                                                    <p><a href="#" data-toggle="modal" data-target="#forgotpassword" data-dismiss="modal">Lost your password?</a>.</p>
                                                    <p><a href="#" data-toggle="modal" data-target="#forgotpassword" data-dismiss="modal">Forgot your username or email?</a>.</p>
                                                    <p><a href="#" data-toggle="modal" data-target="#register" data-dismiss="modal">Reopen your account?</a>.</p>
                                                </div><!--end of passwors-status-div-->
                                                <div class="account-status-div clearfix">
                                                    <p>Don't have an account?<a href="#" data-toggle="modal" data-target="#register" data-dismiss="modal"> Sign up here.</a>.</p>
                                                </div><!--end of passwors-status-div-->
                                                <div class="update-me clearfix">
                                                    <div class="col-md-12"><span><input type="checkbox" name="_spring_security_remember_me">Remenber Me</span></div>
                                                    <div class="col-md-12"><span><input type="checkbox">Keep me update via e-mail.</span></div>
                                                </div>
                                            </div><!--end of row-->
                                        </div><!--end of register-form-->
                                    </div><!-- end of login-regi-popup -->
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-login" id="login-btn">Log in</button>
                                    <button type="button" class="btn btn-clear" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Login Modal end -->

                    <!-- register Modal start -->
                    <div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-421">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                                    <h4 class="modal-title">Register</h4>
                                </div>
                                <div class="scrollable-pop">
                                    <div class="modal-body">
                                        <div class="login-regi-popup clearfix">
                                            <a class="btn facebook-btn" href="#"><i class="fa fa-facebook"> </i> Connect with Facebook</a>
                                            <a class="btn google-btn" href="#"><i class="fa fa-google-plus"> </i> Connect with Google</a>
                                            <h2 class="col-title"><span> or</span></h2>
                                            <div class="register-form clearfix">
                                                <div class="row">
                                                    <center><div style="font-size: 12px; color: red;" id="ajax_reg_error_firstname"></div></center>
                                                    <div class="col-md-6"><input id="firstName" type="text" placeholder="First Name" class="name"></div>
                                                    <div class="col-md-6"><input id="lastName" type="text" placeholder="Last Name" class="name"></div>
                                                    <div class="col-md-12"><input id="user_name" type="text" placeholder="User Name" class="fullwidth  icon-if-user"></div><div style="font-size: 12px; color: red;" id="ajax_reg_error_username"></div>
                                                    <div class="col-md-12"><input id="email_add" type="email" placeholder="Email Address" class="fullwidth  icon-email"></di<div style="font-size: 12px; color: red;" id="ajax_reg_error_email"></div>
                                                        <div class="col-md-12"><input id="pass_1" type="password" placeholder="Password" class="fullwidth  icon-password"></div>
                                                        <div style="font-size: 12px; color: red;" id="ajax_reg_error_pass1"></div>
                                                        <div class="col-md-12"><input id="pass_2" type="password" placeholder="Confrim Password" class="fullwidth  icon-password"></div>
                                                        <div style="font-size: 12px; color: red;" id="ajax_reg_error_pass2"></div>
                                                        <p class="confirmation clearfix">By clicking Register, you confirm that you accept our <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a>.</p>
                                                        <div class="col-md-12"><span class="checkbox"><input type="checkbox" id="send-newsleter" value="yes">I want to receive Kindoff-Electric, an email news letter of fresh trends and editors' picks.</span></div>

                                                    </div><!--end of row-->
                                                </div><!--end of register-form-->
                                            </div><!-- end of login-regi-popup -->
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn login-link" data-dismiss="modal">Login</button>
                                            <button type="button" class="btn btn-login" id="reg-btn">Register</button>
                                            <button type="button" class="btn btn-clear" id="cancel-btn" data-dismiss="modal">Cancel</button>
                                        </div>
                                    </div><!-- end of scrollable -->
                                </div>
                            </div>
                        </div>
                        <!-- register Modal end -->

                        <!-- forgot passwrod Modal start -->
                        <div class="modal fade" id="forgotpassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-421">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                                        <h4 class="modal-title" id="forget-pass-Label">Forgot Password</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="login-regi-popup clearfix">
                                            <div class="register-form clearfix">
                                                <div class="row">
                                                    <div style="font-size: 12px; color: red;" id="ajax_reg_error_back_email"></div>
                                                    <div class="col-md-12"><input id="email_id" type="text" placeholder="Email Address" class="fullwidth  icon-email"></div>
                                                </div><!--end of row-->
                                            </div><!--end of register-form-->
                                        </div><!-- end of login-regi-popup -->
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-login" id="reset-btn">Submit</button>
                                        <button type="button" class="btn btn-clear" data-dismiss="modal" id="rest-cancel-btn">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- forgot passwrod Modal end -->

                        <!--  reset password Model start -->

                        <div class="modal fade" id="reset-password" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-421">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                                        <h4 class="modal-title" id="resetPsssLabel">Reset Password</h4>
                                    </div>
                                    <div class="modal-body">
                                        <input type="hidden" id="conf_id"/>
                                        <input type="hidden" id="forgot_user_name"/>

                                        <div class="login-regi-popup clearfix">
                                            <div class="register-form clearfix">
                                                <div class="row">
                                                    <div class="col-md-12"><input id="res_pass_1" type="password" placeholder="Password" class="fullwidth  icon-password"></div>
                                                    <div style="font-size: 12px; color: red;" id="ajax_reg_error_res_pass1"></div>
                                                    <div class="col-md-12"><input id="res_pass_2" type="password" placeholder="Confrim Password" class="fullwidth  icon-password"></div>
                                                    <div style="font-size: 12px; color: red;" id="ajax_reg_error_res_pass2"></div>
                                                </div><!--end of row-->
                                            </div><!--end of register-form-->
                                        </div><!-- end of login-regi-popup -->
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-login" id="reset-pass-btn">Submit</button>
                                        <button type="button" class="btn btn-clear" data-dismiss="modal" id="reset-pass-cancel-btn">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--  reset password Model end -->



                        <!--<ul class="pull-right">
                            <li class="header-cart">
                                <button class="btn btn-link header-item-switcher"><span class="i-cart"></span></button>
                                <div class="header-item-content">
                                    <div class="carousel-cart" style="width:392px">
                                        <div class="item">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <div class="product-list-item inverted">
                                                        <a href="/big" class="product-list-description">
                                                            <img src="img/square-placeholder.png" style="background-image:url(http://lorempixel.com/472/472/fashion/4);" class="img-responsive" alt="">
                                                            <span><b>Disrupt hella cray Thundercats meditation chambray slow cray Thundercats meditation chambray slow</b></span>
                                                        </a>
                                                        <div class="product-list-seller">
                                                            <a href="#" class="user-rating">
                                                                <span class="i-rating-inverted"></span> 6.9
                                                            </a>
                                                            <a href="#" class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/2" alt=""></a>
                                                        </div>
                                                        <div class="product-list-price"><s>$12</s> $7</div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6">
                                                    <div class="product-list-item inverted">
                                                        <a href="/big" class="product-list-description">
                                                            <img src="img/square-placeholder.png" style="background-image:url(http://lorempixel.com/472/472/fashion/5);" class="img-responsive" alt="">
                                                            <span><b>Blue Bottle meggings tofu mustache. Shoreditch wolf meditation chambray slow</b></span>
                                                        </a>
                                                        <div class="product-list-seller">
                                                            <a href="#" class="user-rating">
                                                                <span class="i-rating-inverted"></span> 6.9
                                                            </a>
                                                            <a href="#" class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/3" alt=""></a>
                                                        </div>
                                                        <div class="product-list-price"><s>$12</s> $7</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <div class="product-list-item inverted">
                                                        <a href="/big" class="product-list-description">
                                                            <img src="img/square-placeholder.png" style="background-image:url(http://lorempixel.com/472/472/fashion/4);" class="img-responsive" alt="">
                                                            <span><b>Disrupt hella cray Thundercats meditation chambray slow cray Thundercats meditation chambray slow</b></span>
                                                        </a>
                                                        <div class="product-list-seller">
                                                            <a href="#" class="user-rating">
                                                                <span class="i-rating-inverted"></span> 6.9
                                                            </a>
                                                            <a href="#" class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/2" alt=""></a>
                                                        </div>
                                                        <div class="product-list-price"><s>$12</s> $7</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="submit-area clearfix">
                                        <button class="btn btn-default inverted pull-right">CHECK OUT</button>
                                        <div class="margin-tiny"></div>
                                        3 items, grand-total: $56
                                    </div>
                                </div>
                            </li>
                            <li class="header-profile">
                                <button class="btn btn-link header-item-switcher">
                                    <span class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/1" alt=""></span>
                                    <small class="hidden-xs">Marlee Terra</small>
                                </button>
                                <div class="header-item-content list-group">
                                    <a href="#" class="list-group-item">View Profile</a>
                                    <a href="#" class="list-group-item">Edit Profile</a>
                                    <a href="#" class="list-group-item">Purchase History</a>
                                    <a href="#" class="list-group-item">Messages <span class="badge">4</span></a>
                                    <a href="#" class="list-group-item">Responses</a>
                                </div>
                            </li>
                        </ul>-->
                    </div>
                </div>
            </div>
    </nav>
    <div class="container">
        <div class="row">
            <div class="col-sm-3 col-lg-2 hidden-xs">
                <div class="list-group">
                    <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Diapering</a>
                    <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>On the Go</a>
                    <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Activities & Play</a>
                    <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Baby Care</a>
                    <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Nursery</a>
                    <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Feeding</a>
                    <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>For Mom</a>
                    <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Health & Safety</a>
                </div>
                <button class="btn btn-md btn-default btn-wide"><span class="i-bubble-inverted"></span> <span class="hidden-sm">Parent's Commiunity</span></button>
                <div class="margin-tiny"></div>
                <button id="tradeButton-2" class="btn btn-md btn-default btn-wide"><span class="i-add-inverted"></span> Trade Your Stuff</button>
            </div>
            <div class="col-sm-9 col-lg-10">
                <h1 class="text-center">A community of Smart Buying and Selling by Moms Worldwide</h1>
                <div class="margin-small"></div>
                <div class="carousel-main">
                    <div class="item" style="width:498px"><img src="http://lorempixel.com/992/496/fashion/1" alt=""></div>
                    <div class="item" style="width:498px"><img src="http://lorempixel.com/992/496/fashion/2" alt=""></div>
                    <div class="item" style="width:498px"><img src="http://lorempixel.com/992/496/fashion/3" alt=""></div>
                </div>
                <div class="margin-medium"></div>
                <div class="visible-xs">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="list-group">
                                <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Diapering</a>
                                <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>On the Go</a>
                                <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Activities & Play</a>
                                <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Baby Care</a>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="list-group">
                                <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Nursery</a>
                                <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Feeding</a>
                                <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>For Mom</a>
                                <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Health & Safety</a>
                            </div>
                        </div>
                    </div>
                    <div class="margin-medium"></div>
                </div>
                <h1 class="text-center">Shop directly from people around the world.</h1>
                <div class="margin-small"></div>
                <div class="row">
                    <div class="col-xs-6 col-sm-4 col-md-3">
                        <div class="product-list-item">
                            <a href="/big" class="product-list-description">
                                <img src="img/square-placeholder.png" style="background-image:url(http://lorempixel.com/472/472/fashion/4);" class="img-responsive" alt="">
                                <span><b>Disrupt hella cray Thundercats meditation chambray slow cray Thundercats meditation chambray slow</b></span>
                            </a>
                            <div class="product-list-seller">
                                <a href="#" class="user-rating">
                                    <span class="i-rating-inverted"></span> 6.9
                                </a>
                                <a href="#" class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/2" alt=""></a>
                            </div>
                            <div class="product-list-price"><s>$12</s> $7</div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3">
                        <div class="product-list-item">
                            <a href="/big" class="product-list-description">
                                <img src="img/square-placeholder.png" style="background-image:url(http://lorempixel.com/472/472/fashion/5);" class="img-responsive" alt="">
                                <span><b>Blue Bottle meggings tofu mustache. Shoreditch wolf meditation chambray slow</b></span>
                            </a>
                            <div class="product-list-seller">
                                <a href="#" class="user-rating">
                                    <span class="i-rating-inverted"></span> 6.9
                                </a>
                                <a href="#" class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/3" alt=""></a>
                            </div>
                            <div class="product-list-price"><s>$12</s> $7</div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3">
                        <div class="product-list-item">
                            <a href="/big" class="product-list-description">
                                <img src="img/square-placeholder.png" style="background-image:url(http://lorempixel.com/472/472/fashion/6);" class="img-responsive" alt="">
                                <span><b>Messenger bag paleo mustache Pinterest. Schlitz meditation chambray slow</b></span>
                            </a>
                            <div class="product-list-seller">
                                <a href="#" class="user-rating">
                                    <span class="i-rating-inverted"></span> 6.9
                                </a>
                                <a href="#" class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/4" alt=""></a>
                            </div>
                            <div class="product-list-price"><s>$12</s> $7</div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 hidden-sm">
                        <div class="product-list-item">
                            <a href="/big" class="product-list-description">
                                <img src="img/square-placeholder.png" style="background-image:url(http://lorempixel.com/472/472/fashion/7);" class="img-responsive" alt="">
                                <span><b>Schlitz distillery meditation mumblecore. Four loko meditation chambray slow</b></span>
                            </a>
                            <div class="product-list-seller">
                                <a href="#" class="user-rating">
                                    <span class="i-rating-inverted"></span> 6.9
                                </a>
                                <a href="#" class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/5" alt=""></a>
                            </div>
                            <div class="product-list-price"><s>$12</s> $7</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="margin-large"></div>
    <footer class="hidden-xs">
        <div class="margin-medium"></div>
        <div class="container">
            <div class="row">
                <div class="col-sm-3 col-md-2">
                    Join the Community
                    <ul class="list-unstyled">
                        <li><a href="#">Teams</a></li>
                        <li><a href="#">Forums</a></li>
                        <li><a href="#">Upcoming Events</a></li>
                        <li><a href="#">Affiliates</a></li>
                    </ul>
                </div>
                <div class="col-sm-3 col-md-2">
                    Discover and Shop
                    <ul class="list-unstyled">
                        <li><a href="#">Gift Cards</a></li>
                        <li><a href="#">Blog</a></li>
                        <li><a href="#">Mobile Apps</a></li>
                        <li><a href="#">Wholesale</a></li>
                    </ul>
                </div>
                <div class="col-sm-3 col-md-2">
                    Get to Know Us
                    <ul class="list-unstyled">
                        <li><a href="#">About</a></li>
                        <li><a href="#">Careers</a></li>
                        <li><a href="#">Press</a></li>
                        <li><a href="#">Developers</a></li>
                    </ul>
                </div>
                <div class="col-sm-3 col-md-2">
                    Discover and Shop
                    <ul class="list-unstyled">
                        <li><a href="#">Gift Cards</a></li>
                        <li><a href="#">Blog</a></li>
                        <li><a href="#">Mobile Apps</a></li>
                        <li><a href="#">Wholesale</a></li>
                    </ul>
                </div>
                <div class="col-sm-5 col-md-4 col-lg-3 col-lg-offset-1">
                    <div class="margin-medium visible-sm"></div>
                    <h4>Join Newsletter</h4>
                    <div class="margin-small"></div>

                    <div class="input-group">
                        <label for="Email" class="sr-only">Email address</label>
                        <input id="subscribe-mob-box" type="email" class="form-control input-sm" id="Email" placeholder="your@email.com">
                        <span class="input-group-btn"><button id="subscribe-mob-submit" type="submit" class="btn btn-sm btn-primary">SUBSCRIBE</button></span>
                    </div>

                    <div class="margin-small"></div>
                    <div class="icons-row">
                        <a href="https://twitter.com/kindoff_co" target="_blank"><span class="i-twitter"></span></a>
                        <a href="https://www.facebook.com/kindoff.co" target="_blank"><span class="i-facebook"></span></a>
                        <a href="http://instagram.com/kindoff.co" target="_blank"><span class="i-instagram"></span></a>
                    </div>
                    <div class="margin-tiny"></div>
                    &copy; 2015 Kindoff. Ltd
                </div>
            </div>
        </div>
        <div class="margin-medium"></div>
    </footer>
    <div class="visible-xs small text-center text-muted">
        &copy; 2015 Kindoff. Ltd
        <div class="margin-medium"></div>
    </div>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>