<%--
  Created by IntelliJ IDEA.
  User: darshana
  Date: 04/04/15
  Time: 6:03 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Index</title>
    <script type="application/javascript" src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
    <script src="js/jquery.easing.1.3.min.js"></script>
    <style>

        li {
            list-style: none;
            margin-top: 20px;
        }

        li * {
            vertical-align: middle;
            font-family: sans-serif;
            font-size: small;
        }

        form input[type="text"],input[type="reset"],input[type="submit"],select,textarea {
            border: solid thin;
        }

        form input[type="submit"] {
            position: relative;left: 30%
        }

        form input[type="reset"]{
            position: relative;left: 35%;
        }

        #main-div{
            border:solid thin;
            width: 50%;
            height: auto;
            margin: auto 23%;
            position: absolute;
        }

        #message-panel{
            border: solid thin;
            margin: 20px;
        }

        #message-panel label[for="recievers"] {
            margin-right: 10px;
        }

        #message-panel label[for="subject"] {
            margin-right: 25px;
        }

        #message-panel input[id="recievers"],input[id="subject"] {
            width: 70%;
        }

        #message-list li {
            margin-top: 10px;
        }

        #message-list li * {
            text-align: left;
            vertical-align: text-bottom;
        }

        .subject-line{
            background-color: #99FF99;
            width: 89%;
            height: 30px;
            padding-bottom: 10px;
        }

        .message-body {
            border: solid thin;
            margin: 10px;
            width: 89%;
            height: auto;
            display: none;
        }

        .reply-btn-class {
            border: solid thin;
        }

        .profile-pic {
            border: solid thin;
            height: 30px;
            width: 30px;
            border-radius: 100%;
            margin-right: 10px;
        }

        #message-list {
            margin-bottom: 5%;
        }

    </style>
    <script>

        $(document).ready(function(){

            $(".subject-line").click(function(){
                $(this).next().slideToggle("fast","easeInOutSine");
            });

            $(".reply-btn-class").click(function(){
                $(this).parent().next().slideToggle("fast","easeInOutSine");
            });

        });

    </script>
</head>
<body>

<div id="main-div">

    <div id="message-panel">
        <form>
            <ul>
                <li>
                    <label for="recievers">Receivers</label>
                    <input id="recievers" type="text">
                </li>
                <li>
                    <label for="subject">Subject</label>
                    <input id="subject" type="text">
                </li>
                <li>
                    <label for="message">Message</label><br>
                    <textarea id="message" cols="60%" rows="10"></textarea>
                </li>
                <li>
                    <input type="submit" id="send-message" value="Send">
                    <input type="reset">
                </li>
            </ul>
        </form>
    </div>

    <div id="message-list">
        <ul>
            <li>
                <div class="subject-line"><img src="http://lorempixel.com/64/64/people/1" class="profile-pic"/>Silva says : Your stuffs are excellent</div>
                <div class="message-body">
                    <ul>
                        <li><input class="reply-btn-class" type="button" value="Reply"></li>
                        <li style="display: none">
                            <label>To : darshanadsw@gmail.com</label>
                            <textarea cols="50%" rows="10"></textarea><br>
                            <input type="button" value="Send"/>
                        </li>
                        <li>
                            From : me <br>
                            Sent At : 2015 / 04 / 04 1.45 <br><br>

                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and scrambled it to make a
                        </li>
                        <li>
                            From : Silva <br>
                            Sent At : 2015 / 04 / 04 23.45 <br><br>

                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and scrambled it to make a
                            type specimen book. It has survived not only five centuries, but also the leap
                            into electronic typesetting, remaining essentially unchanged. It was popularised
                            in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                            and more recently with desktop
                            publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <div class="subject-line"><img src="http://lorempixel.com/64/64/people/1" class="profile-pic"/>Silva says : Your stuffs are excellent</div>
                <div class="message-body">
                    <ul>
                        <li><input class="reply-btn-class" type="button" value="Reply"></li>
                        <li style="display: none">
                            <label>To : darshanadsw@gmail.com</label>
                            <textarea cols="50" rows="10"></textarea><br>
                            <input type="button" value="Send"/>
                        </li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and scrambled it to make a
                            type specimen book. It has survived not only five centuries, but also the leap
                            into electronic typesetting, remaining essentially unchanged. It was popularised
                            in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                            and more recently with desktop
                            publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                        </li>
                    </ul>
                </div>

            </li>
            <li>
                <div class="subject-line"><img src="http://lorempixel.com/64/64/people/1" class="profile-pic"/>Silva says : Your stuffs are excellent</div>
                <div class="message-body">
                    <ul>
                        <li><input class="reply-btn-class" type="button" value="Reply"></li>
                        <li style="display: none">
                            <label>To : darshanadsw@gmail.com</label>
                            <textarea cols="50" rows="10"></textarea><br>
                            <input type="button" value="Send"/>
                        </li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and scrambled it to make a
                            type specimen book. It has survived not only five centuries, but also the leap
                            into electronic typesetting, remaining essentially unchanged. It was popularised
                            in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                            and more recently with desktop
                            publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                        </li>
                    </ul>
                </div>

            </li>
        </ul>
    </div>


</div>

</body>
</html>
