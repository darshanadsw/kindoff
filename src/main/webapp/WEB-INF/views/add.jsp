<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="com.kindoff.portal.util.Constant"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html xml:lang="en" lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Kindoff - A community of Smart Buying and Selling by Moms Worldwide</title>
	<meta name="description" content="Kindoff — A community of Smart Buying and Selling by Moms Worldwide">
	<meta name="keywords" content="Kindoff, baby, kids, children, e-commerce, goods, items">
	<meta name="robots" content="INDEX,FOLLOW">
	<link rel="icon" href="favicon.ico" type="image/x-icon">
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
	<meta property="og:site_name" content="Kindoff">
	<meta property="og:description" content="Kindoff — A community of Smart Buying and Selling by Moms Worldwide">
	<meta property="og:type" content="website">
	<meta property="og:url" content="http://Kindoff.co/">
	<meta property="og:image" content="http://Kindoff.co/img/opengraph/logo.png">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="mobile-web-app-capable" content="yes">
	<link rel="stylesheet" type="text/css" href="css/base.css">
	<link rel="stylesheet" type="text/css" href="css/xcharts.min.css">
	<style type="text/css">
	
	div#spinner
	{
    	display: none;
    	width:100px;
    	height: 100px;
    	position: fixed;
    	top: 50%;
    	left: 50%;
    	background:url(img/preloader.gif) no-repeat center;
    	text-align:center;
    	padding:10px;
    	font:normal 16px Tahoma, Geneva, sans-serif; 
    	z-index:2;
    	overflow: auto;
	}
	
	.image-upload > input
	{
    	display: none;
	}
	
	.error {
  		box-shadow: 0 0 5px rgba(255, 0, 0, 1);
  		padding: 3px 0px 3px 3px;
  		margin: 5px 1px 3px 0px;
  		border: 1px solid rgba(255, 0, 0, 1);
	}
	
	</style>
	<script src="js/jquery-2.1.1.min.js"></script>
	<script src="js/modernizr.js"></script>
	<script src="js/retina.js"></script>
	<script src="js/custom.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/xcharts.min.js"></script>
	<script src="js/d3.min.js"></script>
	<script src="js/portalutils.js"></script>
	<script src="js/vendor/jquery.ui.widget.js"></script>
	<script src="js/jquery.iframe-transport.js"></script>
	<script src="js/jquery.fileupload.js"></script>
	
	<script type="text/javascript">
		
		$('document').ready(function(){
			
			var generateProductUUID = function () {
				function chunk_4(){
					return Math.floor(Math.random()*10000)+"";
				}
				var set1 = chunk_4();
				var set2 = chunk_4();
				var set3 = chunk_4();
				var set4 = chunk_4();
				
				return set1.concat("-").concat(set2).concat("-").concat(set3).concat("-").concat(set4);				
			};
			
			var uuid = generateProductUUID();
			
			
				loadKeyValueByType('<%=Constant.AGE_LIST%>','age-drop-box');
				loadKeyValueByType('<%=Constant.CONDITION_LIST%>','condition-drop-box');
			
			$.ajax({
		        type: "GET",
		        url: "loadCountryList",
		        data:{},
		        contentType: "application/json",              
		        dataType: "json",
		        success: function (arten) {
		        	$("#countryBox").empty();		        
		        		$.each(arten, function () {
		            		$("#countryBox").append($("<option></option>").val(this['id']).html(this['countryName']));
		        		});
		        		
		        		loadStates($("#countryBox option:first").val());
		        		
		    	 	}
		  	});
			
			function loadCities(stateId){
				$.ajax({
			        type: "GET",
			        url: "loadCityList/"+stateId,
			        data:{},
			        contentType: "application/json",              
			        dataType: "json",
			        success: function (arten) {
			        $("#city-box").empty();	
			        $("#city-box").append($("<option></option>").val('').html('Any'));
			        $.each(arten, function () {
			            $("#city-box").append($("<option></option>").val(this['id']).html(this['cityName']));
			        });
			     	}
			  	});
			}
			
			function loadStates(countryId){
				$.ajax({
			        type: "GET",
			        url: "loadStateList/"+countryId,
			        data:{},
			        contentType: "application/json",              
			        dataType: "json",
			        success: function (arten) {
			        	$("#stateBox").empty();	
			        	$("#stateBox").append($("<option></option>").val('').html('Any'));
			        	$.each(arten, function () {
			            	$("#stateBox").append($("<option></option>").val(this['id']).html(this['stateName']));
			        	});
			     	}
			  	});
			}
			
			$("#countryBox").change(function (){
				loadStates($(this).val());
				loadCities(-1);
			});
			
			$("#stateBox").change(function (){
				loadCities($(this).val());
			});
			
			
				var deliveryMethod='';
				var payMethod;
				
				$("#del-method-picup").click(function(){
					$('#del-method-courier').removeAttr('checked');
					$('#del-method-picup').attr('checked');
					
					deliveryMethod = $('#del-method-picup').val();
				});
				
				$("#del-method-courier").click(function(){
					$('#del-method-courier').attr('checked');
					$('#del-method-picup').removeAttr('checked');
					
					deliveryMethod = $('#del-method-courier').val();
				});
				
				$("#pay-method-pal").click(function(){
					$('#pay-method-pal').attr('checked');
					$('#pay-method-cash').removeAttr('checked');
					
					payMethod = $('#pay-method-pal').val();
				});
				
				$("#pay-method-cash").click(function(){
					$('#pay-method-cash').attr('checked');
					$('#pay-method-pal').removeAttr('checked');
					
					payMethod = $('#pay-method-cash').val();
				});
							
			
			$("#post-my-stuff").click(function(){
				// add item				
				if(validateSubmit()){
		            var json = { 	
		            		"productLocation" : $("#countryBox").val(),
            				"description" : $("#desc-box").val(),
            				"initialPrice": $("#price-box").val(),
            				"sex" :$("#sex-box").val(),
            				"age" : $("#age-drop-box").val(),
            				"condition" : $("#condition-drop-box").val(),
            				"deliveryMethodId" : deliveryMethod,
            				"paymentMethodId" : payMethod,
            				"quantity" : $("#quantity-box").val(),
            				"currency" : $("#curr-btn").val(),
            				"stateId" : $("#stateBox").val(),
            				"cityId" : $("#city-box").val(),
            				"productUUID" : uuid
            		};

			
		$.ajax({ 
                url:"addItem",    
                type:"POST", 
                contentType: "application/json",
                data: JSON.stringify(json), //Stringified Json Object					
                async: false,    //Cross-domain requests and dataType: "jsonp" requests do not support synchronous operation
                cache: false,    //This will force requested pages not to be cached by the browser          
                processData:false, //To avoid making query String instead of JSON
                beforeSend: function(xhr) {
                	$("#spinner").show();
                	$("#post-my-stuff").attr("disabled");
                },
                success: function(resposeJsonObject){   
                	$("#spinner").hide();
       		 		alert(resposeJsonObject.message);
       		 		$("#productId").val(resposeJsonObject.id);       		 		
       		 		$("#post-my-stuff").removeAttr("disabled");
       		 	    uuid = generateProductUUID();
       		 	    
       		 	    // reset 
       		 	$("#countryBox").val($("#countryBox option:first").val());
				$("#desc-box").val("");
				$("#price-box").val("");
				$("#sex-box").val("");
				$("#age-drop-box").val("")
				$("#condition-drop-box").val("");
				$('#del-method-courier').removeAttr('checked');
				$('#del-method-picup').removeAttr('checked');
				$('#pay-method-pal').removeAttr('checked');
				$('#pay-method-cash').removeAttr('checked');
				$("#quantity-box").val(1);
				$("#curr-btn").val("CAD");
				$("#stateBox").val("");
				$("#city-box").val("");
								
				$("#fig-1").html("<div class='margin-small' id='marg-1'></div><div class='image-upload'><label for='file-input-1'><div id ='lab-1'><span class='i-add'><img id='img-1'/></span></div></label><input id='file-input-1' type='file'/></div><div id='add-1'>add photos</div><div class='margin-tiny'></div>");	
				$("#fig-2").html("<div class='margin-small' id='marg-2'></div><div class='image-upload'><label for='file-input-2'><div id ='lab-2'><span class='i-add'><img id='img-2'/></span></div></label><input id='file-input-2' type='file'/></div><div id='add-2'>add photos</div><div class='margin-tiny'></div>");
				$("#fig-3").html("<div class='margin-small' id='marg-3'></div><div class='image-upload'><label for='file-input-3'><div id ='lab-3'><span class='i-add'><img id='img-3'/></span></div></label><input id='file-input-3' type='file'/></div><div id='add-3'>add photos</div><div class='margin-tiny'></div>");
				
       		 	    
               	}               	
			});
				}
			});
			
			// file upload - 3
			
			$('#file-input-3').click(function (){				
				
					var f3 = $("#lab-3").html();
					$("#lab-3").html("<span ><img src='img/preloader.gif'></img></span>");
					
					$('#file-input-3').fileupload({
		        		dataType: 'json',
		        		url: 'upload/'+uuid+"/3",		        		
		        		success : function (result, textStatus, jqXHR) {
		        			if(result.responseType == 'error'){
		        				alert(result.message);
		        				$("#lab-3").html('');
		        				$("#lab-3").html(f3);
		        				
		        			}else{
		        				$("#marg-3").removeClass("margin-small");
		        				$("#add-3").hide();
			        			$("#lab-3").html('');	
			        			podId = $("#productId").val();
			        			ex = result.holder.fileExtension;
			        			$("#lab-3").html("<span ><img src='uploadImages/"+uuid+"/3."+ex+"?" +  new Date().getTime() +"' width=80px></img></span>");
		        			}		        			
		        		},
		        		error : function (jqXHR, textStatus, errorThrown) {
		        			
		        		},
		        		complete : function (result, textStatus, jqXHR) {
		        			
		        		}
		    		});				
				
			});
			
			// file upload - 2
			
			$('#file-input-2').click(function (){
							
					
					var f2 = $("#lab-2").html();
					$("#lab-2").html("<span ><img src='img/preloader.gif'></img></span>");
					
					$('#file-input-2').fileupload({
		        		dataType: 'json',
		        		url: 'upload/'+uuid+"/2",		        		
		        		success : function (result, textStatus, jqXHR) {
		        			if(result.responseType == 'error'){
		        				alert(result.message);
		        				$("#lab-2").html('');
		        				$("#lab-2").html(f2);
		        			}else{
		        				$("#marg-2").removeClass("margin-small");
		        				$("#add-2").hide();
			        			$("#lab-2").html('');		        			
			        			podId = $("#productId").val();
			        			ex = result.holder.fileExtension;
			        			$("#lab-2").html("<span ><img src='uploadImages/"+uuid+"/2."+ex+"?"+  new Date().getTime() +"' width=80px></img></span>");
		        			}		        			
		        		},
		        		error : function (jqXHR, textStatus, errorThrown) {
		        			
		        		},
		        		complete : function (result, textStatus, jqXHR) {
		        			
		        		}
		    		});
				
				
			});
			
			// file upload - 1
			
			$('#file-input-1').click(function (){
									
					var f1 = $("#lab-1").html();
					$("#lab-1").html("<span ><img src='img/preloader.gif'></img></span>");
					
					
					$('#file-input-1').fileupload({
		        		dataType: 'json',
		        		url: 'upload/'+uuid+"/1",		        		
		        		success : function (result, textStatus, jqXHR) {
		        			if(result.responseType == 'error'){
		        				alert(result.message);
		        				$("#lab-1").html('');
		        				$("#lab-1").html(f1);		        				
		        			}else{
		        				$("#marg-1").removeClass("margin-small");
		        				$("#add-1").hide();
			        			$("#lab-1").html('');
			        			podId = $("#productId").val();
			        			ex = result.holder.fileExtension;
			        			$("#lab-1").html("<span ><img src='uploadImages/"+uuid+"/1."+ex+"?"+  new Date().getTime() +"' width=80px></img></span>");
		        			}
		        		},
		        		error : function (jqXHR, textStatus, errorThrown) {
		        			
		        		},
		        		complete : function (result, textStatus, jqXHR) {
		        			
		        		}
		    		});
								
			});
			
			// currencu drop down
			$("#usd").click(function () {
				$("#curr-btn").val('USD');
				$("#curr-btn").html('USD <span class="caret"></span>');				
			});
			$("#cad").click(function (){
				$("#curr-btn").val('CAD');
				$("#curr-btn").html('CAD <span class="caret"></span>');
			});
			
			/// validations
			
			function validateSubmit(){
				var flag = true;				
				if($("#quantity-box").val() == ''){
					$("#quantity-box").addClass("error");
					flag = false;
				}
				if($("#price-box").val() == ''){
					$("#price-box").addClass("error");
					flag = false;
				}
				if($("#countryBox").val() == ''){
					$("#countryBox").addClass("error");
					flag = false;
				}
				if($("#stateBox").val() == ''){
					$("#stateBox").addClass("error");
					flag = false;
				}
				if($("#city-box").val() == ''){
					$("#city-box").addClass("error");
					flag = false;
				}
				
				if(!$("#del-method-picup").is(':checked') && !$("#del-method-courier").is(':checked')){
					$("#del-method-picup").addClass('error');	
					$("#del-method-courier").addClass('error');
					flag = false;
				}	
				
				if(!$("#pay-method-cash").is(':checked') && !$("#pay-method-pal").is(':checked')){
					$("#pay-method-cash").addClass('error');	
					$("#pay-method-pal").addClass('error');
					flag = false;
				}					
				
				return flag;
			}
			
			$("#quantity-box").on('blur', function(){
				if($(this).val() != ''){
					$("#quantity-box").removeClass('error');
				}
			});
			
			$("#price-box").on('blur', function(){
				if($(this).val() != ''){
					$("#price-box").removeClass('error');
				}
			});
			
			$("#countryBox").on('blur', function(){
				if($(this).val() != ''){
					$("#countryBox").removeClass('error');
				}
			});
			
			$("#stateBox").on('blur', function(){
				if($(this).val() != ''){
					$("#stateBox").removeClass('error');
				}
			});
			$("#city-box").on('blur', function(){
				if($(this).val() != ''){
					$("#city-box").removeClass('error');
				}
			});
			
			$("#del-method-picup").on('click', function(){				
				$("#del-method-picup").removeClass('error');	
				$("#del-method-courier").removeClass('error');
			});
			
			$("#del-method-courier").on('click', function(){				
				$("#del-method-picup").removeClass('error');	
				$("#del-method-courier").removeClass('error');
			});
			
			$("#pay-method-cash").on('click', function(){				
				$("#pay-method-cash").removeClass('error');	
				$("#pay-method-pal").removeClass('error');
			});
			
			$("#pay-method-pal").on('click', function(){				
				$("#pay-method-cash").removeClass('error');	
				$("#pay-method-pal").removeClass('error');
			});
			
			
			// subscription
			$('#subscribe-mob-submit').click( function() {
				if(validateEmail($('#subscribe-mob-box').val())) {
					$.ajax({
		        		url: 'subscribe',
		       			 type: 'post',
		        		contentType: "application/json",
		        		dataType: 'json',
		        		data: JSON.stringify({'subscriberEmail' : $('#subscribe-mob-box').val()}),
		        		success: function(data) {
		            		alert(data.message);  
		            		$('#subscribe-mob-box').val('');
		        		}
		    		});
				}	    		
			});
			
			$('#subscribe-des-submit').click( function() {
				if(validateEmail($('#subscribe-des-box').val())) {
					$.ajax({
		        		url: 'subscribe',
		       			 type: 'post',
		        		contentType: "application/json",
		        		dataType: 'json',
		        		data: JSON.stringify({'subscriberEmail' : $('#subscribe-des-box').val()}),
		        		success: function(data) {
		            		alert(data.message);    
		            		$('#subscribe-des-box').val('');
		        		}
		    		});
				}	    		
			});			
			
		});
	
	</script>
</head>
<body>

	<div id="spinner">
        Loading...
    </div>
<input type="hidden" id="productId"/>   
<div class="mobile-menu">
	<a href="#" id="mobile-menu-close"><span class="i-close-highlight"></span> <small class="text-primary">CLOSE</small></a>
	<div class="margin-small"></div>
	<div class="list-group">
	  <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Diapering</a>
	  <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>On the Go</a>
	  <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Activities & Play</a>
	  <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Baby Care</a>
	  <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Nursery</a>
	  <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Feeding</a>
	  <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>For Mom</a>
	  <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Health & Safety</a>
	</div>
	<button class="btn btn-md btn-default btn-wide"><span class="i-bubble-inverted"></span> <span class="hidden-sm">Parent’s</span> Community</button>
	<div class="margin-tiny"></div>
	<button class="btn btn-md btn-default btn-wide"><span class="i-add-inverted"></span> Trade Your Stuff</button>
	<div class="margin-medium"></div>
	Join the Community
	<ul class="list-unstyled">
		<li><a href="#">Teams</a></li>
		<li><a href="#">Forums</a></li>
		<li><a href="#">Upcoming Events</a></li>
		<li><a href="#">Affiliates</a></li>
	</ul>
	<div class="margin-medium"></div>
	Discover and Shop
	<ul class="list-unstyled">
		<li><a href="#">Gift Cards</a></li>
		<li><a href="#">Blog</a></li>
		<li><a href="#">Mobile Apps</a></li>
		<li><a href="#">Wholesale</a></li>
	</ul>
	<div class="margin-medium"></div>
	Get to Know Us
	<ul class="list-unstyled">
		<li><a href="#">About</a></li>
		<li><a href="#">Careers</a></li>
		<li><a href="#">Press</a></li>
		<li><a href="#">Developers</a></li>
	</ul>
	<div class="margin-medium"></div>
	Discover and ShopcountryBox
	<ul class="list-unstyled">
		<li><a href="#">Gift Cards</a></li>
		<li><a href="#">Blog</a></li>
		<li><a href="#">Mobile Apps</a></li>
		<li><a href="#">Wholesale</a></li>
	</ul>
	<div class="margin-small"></div>
	<hr>
	<div class="margin-small"></div>
	<h4>Join Newsletter</h4>
	<div class="margin-small"></div>
	<!-- <form> -->
		<div class="input-group">
			<label for="Email" class="sr-only">Email address</label>
			<input id="subscribe-des-box" type="email" class="form-control input-sm" placeholder="your@email.com">
			<span class="input-group-btn"><button id="subscribe-des-submit" type="submit" class="btn btn-sm btn-primary">SUBSCRIBE</button></span>
		</div>
	<!-- </form> -->
	<div class="margin-small"></div>
	<div class="icons-row">
		<a href="https://twitter.com/kindoff_co" target="_blank"><span class="i-twitter"></span></a>
		<a href="https://www.facebook.com/kindoff.co" target="_blank"><span class="i-facebook"></span></a>
		<a href="http://instagram.com/kindoff.co" target="_blank"><span class="i-instagram"></span></a>
	</div>
</div>
<nav class="navbar navbar-default">
	<div class="container">
		<div class="row">
			<div class="col-xs-5 visible-xs header-nav">
				<ul>
					<li><button class="btn btn-link header-mobile-menu" id="mobile-menu-open"><span class="i-menu"></span></button></li>
					<li>
						<button class="btn btn-link header-item-switcher"><span class="i-search"></span></button>
						<div class="header-item-content">
							<div class="input-group">
								<input type="text" class="form-control" placeholder="Search goods..." aria-label="search-global">
								<div class="input-group-btn">
									<button type="button" class="btn btn-transparent dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
										in all categories <span class="i-arrow-down"></span>
										<span class="sr-only">Toggle Dropdown</span>
									</button>
									<ul class="dropdown-menu dropdown-menu-right" role="menu">
										<li><a href="#">Diapering</a></li>
										<li><a href="#">On the Go</a></li>
										<li><a href="#">Activities & Play</a></li>
										<li><a href="#">Baby Care</a></li>
										<li><a href="#">Nursery</a></li>
										<li><a href="#">Feeding</a></li>
										<li><a href="#">For Mom</a></li>
										<li><a href="#">Health & Safety</a></li>
									</ul>
									<button type="button" class="btn btn-default"><span class="i-search-inverted"></span></button>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<div class="col-xs-2">
				<a href="/KindOff"><span class="i-logo-sm hidden-xs"></span></a>
				<div class="text-center visible-xs"><a href="/KindOff"><span class="i-logo-xs"></span></a></div>
			</div>
			<div class="col-sm-6 col-lg-5 hidden-xs">
				<div class="input-group">
					<input type="text" class="form-control" placeholder="Search goods..." aria-label="search-global">
					<div class="input-group-btn">
						<button type="button" class="btn btn-transparent dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
							in all categories <span class="i-arrow-down"></span>
							<span class="sr-only">Toggle Dropdown</span>
						</button>
						<ul class="dropdown-menu dropdown-menu-right" role="menu">
							<li><a href="#">Diapering</a></li>
							<li><a href="#">On the Go</a></li>
							<li><a href="#">Activities & Play</a></li>
							<li><a href="#">Baby Care</a></li>
							<li><a href="#">Nursery</a></li>
							<li><a href="#">Feeding</a></li>
							<li><a href="#">For Mom</a></li>
							<li><a href="#">Health & Safety</a></li>
						</ul>
						<button type="button" class="btn btn-default"><span class="i-search-inverted"></span></button>
					</div>
				</div>
			</div>
			<div class="col-xs-5 col-sm-4 col-lg-5 header-nav">
				<ul class="pull-right">
					<li class="header-cart">
						<button class="btn btn-link header-item-switcher"><span class="i-cart"></span></button>
						<div class="header-item-content">
							<div class="carousel-cart" style="width:392px">
							    <div class="item">
									<div class="row">
										<div class="col-xs-6">
											<div class="product-list-item inverted">
												<a href="/big" class="product-list-description">
													<img src="img/square-placeholder.png" style="background-image:url(http://lorempixel.com/472/472/fashion/4);" class="img-responsive" alt="">
													<span><b>Disrupt hella cray Thundercats meditation chambray slow cray Thundercats meditation chambray slow</b></span>
												</a>
												<div class="product-list-seller">
													<a href="#" class="user-rating">
														<span class="i-rating-inverted"></span> 6.9
													</a>
													<a href="#" class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/2" alt=""></a>
												</div>
												<div class="product-list-price"><s>$12</s> $7</div>
											</div>
										</div>
										<div class="col-xs-6">
											<div class="product-list-item inverted">
												<a href="/big" class="product-list-description">
													<img src="img/square-placeholder.png" style="background-image:url(http://lorempixel.com/472/472/fashion/5);" class="img-responsive" alt="">
													<span><b>Blue Bottle meggings tofu mustache. Shoreditch wolf meditation chambray slow</b></span>
												</a>
												<div class="product-list-seller">
													<a href="#" class="user-rating">
														<span class="i-rating-inverted"></span> 6.9
													</a>
													<a href="#" class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/3" alt=""></a>
												</div>
												<div class="product-list-price"><s>$12</s> $7</div>
											</div>
										</div>
									</div>
								</div>
							    <div class="item">
									<div class="row">
										<div class="col-xs-6">
											<div class="product-list-item inverted">
												<a href="/big" class="product-list-description">
													<img src="img/square-placeholder.png" style="background-image:url(http://lorempixel.com/472/472/fashion/4);" class="img-responsive" alt="">
													<span><b>Disrupt hella cray Thundercats meditation chambray slow cray Thundercats meditation chambray slow</b></span>
												</a>
												<div class="product-list-seller">
													<a href="#" class="user-rating">
														<span class="i-rating-inverted"></span> 6.9
													</a>
													<a href="#" class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/2" alt=""></a>
												</div>
												<div class="product-list-price"><s>$12</s> $7</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="submit-area clearfix">
								<button class="btn btn-default inverted pull-right">CHECK OUT</button>
								<div class="margin-tiny"></div>
								3 items, grand-total: $56
							</div>
						</div>
					</li>
					<li class="header-profile">
						<button class="btn btn-link header-item-switcher">
							<span class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/1" alt=""></span>
							<small class="hidden-xs">
									<security:authorize access="isAuthenticated()">
                                        <security:authentication property="principal.portalUser.firstName" />&nbsp;
                                        <security:authentication property="principal.portalUser.lastName" />
									</security:authorize>
							</small>
						</button>
                        <c:if test="${pageContext.request.userPrincipal.name != null}">

                            <c:url value="/j_spring_security_logout" var="logoutUrl" />
                            <form action="${logoutUrl}" method="post" id="logoutForm">
                                <input type="hidden" name="${_csrf.parameterName}"	value="${_csrf.token}" />
                            </form>

                            <script>
                                function formSubmit() {
                                    document.getElementById("logoutForm").submit();
                                }
                            </script>

                            <div class="header-item-content list-group">
                                <a class="list-group-item" href="dashboard">View Profile</a>
                                <a class="list-group-item" href="editprofile">Edit Profile</a>
                                <a class="list-group-item" href="#">Purchase History</a>
                                <a class="list-group-item" href="#">Messages <span class="badge">4</span></a>
                                <a class="list-group-item" href="#">Responses</a>
                                <a class="list-group-item" href="javascript:formSubmit()">Log Out</a>
                            </div>

                        </c:if>
					</li>
				</ul>
			</div>
		</div>
	</div>
</nav>
<div class="container">
	<div class="form-horizontal">
		<h1>Sell Your Stuff</h1>
		<div class="margin-large"></div>
		<div class="row">
			<div class="col-md-8 col-lg-6">
				<div class="form-group">
					<label for="inputEmail3" class="col-xs-3 control-label">Country</label>
					<div class="col-xs-9">						
						<select class="form-control small" id="countryBox">
						  <option>Any</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-xs-3 control-label">State / Province</label>
					<div class="col-xs-9">						
						<select class="form-control small" id="stateBox">
						  <option>Any</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-xs-3 control-label">City</label>
					<div class="col-xs-9">						
						<select class="form-control small" id="city-box">
						  <option value="">Any</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword3" class="col-xs-3 control-label">Description</label>
					<div class="col-xs-9">
						<textarea class="form-control" rows="6" id="desc-box"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword3" class="col-xs-3 control-label">Price</label>
					<div class="col-xs-9 col-sm-4">
						<div class="input-group">
							<input id="price-box" type="text" class="form-control" aria-label="...">
							<div class="input-group-btn">
								<button  id="curr-btn" type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-expanded="false">USD <span class="caret"></span></button>
								<ul class="dropdown-menu dropdown-menu-right" role="menu">
									<li><a id="usd">USD</a></li>
									<li><a id="cad">CAD</a></li>									
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="margin-medium"></div>
				<div class="form-group small">
					<label for="inputPassword3" class="col-xs-3 control-label">Description</label>
					<div class="col-xs-4">
						<select multiple class="form-control" style="height:170px">
							<option>Diapering</option>
							<option>On the Go</option>
							<option>Activities & Play</option>
							<option>Baby Care</option>
							<option>Nursery</option>
							<option>Feeding</option>
							<option>For Mom</option>
							<option>Health & Safety</option>
						</select>
					</div>
					<div class="col-xs-1 text-center">/</div>
					<div class="col-xs-4">
						<select multiple class="form-control small" style="height:170px">
							<option>Choose a section first</option>
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-lg-3 col-lg-offset-1 small">
				<div class="form-group">
					<label for="inputEmail3" class="col-xs-3 control-label">Sex</label>
					<div class="col-xs-9">
						<select class="form-control small" id="sex-box">
						  <option value="">Any</option>
						  <option value="M">Male</option>
						  <option value="F">Female</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword3" class="col-xs-3 control-label">Age</label>
					<div class="col-xs-9">
						<select class="form-control small" id="age-drop-box">						  
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword3" class="col-xs-3 control-label">Condition</label>
					<div class="col-xs-9">
						<select class="form-control small" id="condition-drop-box">						  					  
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword3" class="col-xs-3 control-label">Delivery</label>
					<div class="col-xs-9">
						<input type="checkbox" id="del-method-picup" value="1"> Pickup <input id="del-method-courier" type="checkbox" value="2"> Courier Service
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword3" class="col-xs-3 control-label">Payments</label>
					<div class="col-xs-9">
						<input type="checkbox" id="pay-method-cash" value="1"> Cash on Pickup <input type="checkbox" id="pay-method-pal" value="2"> PayPal
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword3" class="col-xs-3 control-label">Quantity</label>
					<div class="col-xs-3">
						<input type="text" class="form-control input-md small" id="quantity-box" value="1">
					</div>
				</div>
				<div class="margin-medium"></div>
				<hr>
				<div class="margin-medium"></div>
				<h5 class="text-thin">Photos</h5>
				<div class="margin-small"></div>
				<div class="row add-item-photos">
				
				
					<div class="col-xs-4 col-sm-2 col-md-4">
						<figure id="fig-1">
							
							<div class="margin-small" id="marg-1"></div>
							<div class="image-upload">
    								<label for="file-input-1">
    								<div id ="lab-1">  <span class="i-add"><img id="img-1"/></span> </div>        							
    								</label>
    							<input id="file-input-1" type="file"/>
							</div>
							<div id="add-1">add photos</div>
							<div class="margin-tiny"></div>		
												
						</figure>
					</div>
					
					
					<div class="col-xs-4 col-sm-2 col-md-4">
						<figure id="fig-2">
							
							<div class="margin-small" id="marg-2"></div>
							<div class="image-upload">
    								<label for="file-input-2">
    								<div id ="lab-2">  <span class="i-add"><img id="img-2"/></span> </div>        							
    								</label>
    							<input id="file-input-2" type="file"/>
							</div>
							<div id="add-2">add photos</div>
							<div class="margin-tiny"></div>		
												
						</figure>
					</div>
					
					
					<div class="col-xs-4 col-sm-2 col-md-4">
						<figure id="fig-3">
													
							<div class="margin-small" id="marg-3"></div>
							<div class="image-upload">
    								<label for="file-input-3">
    								<div id ="lab-3">  <span class="i-add"><img id="img-3"/></span> </div>        							
    								</label>
    							<input id="file-input-3" type="file"/>
							</div>
							<div id="add-3">add photos</div>
							<div class="margin-tiny"></div>	
												
						</figure>
					</div>
					
				</div>
			</div>
		</div>
		<div class="margin-medium"></div>
		<div class="cut-out text-center">
			<div class="margin-tiny"></div>
			<button class="btn btn-lg btn-default" id="post-my-stuff">Post Your Stuff</button> &nbsp; &nbsp;
			<button class="btn btn-lg btn-default inverted">Post Your Stuff</button>
			<div class="margin-tiny"></div>
		</div>
	</div>
</div>
<div class="margin-large"></div>
<footer class="hidden-xs">
	<div class="margin-medium"></div>
	<div class="container">
		<div class="row">
			<div class="col-sm-3 col-md-2">
				Join the Community
				<ul class="list-unstyled">
					<li><a href="#">Teams</a></li>
					<li><a href="#">Forums</a></li>
					<li><a href="#">Upcoming Events</a></li>
					<li><a href="#">Affiliates</a></li>
				</ul>	
			</div>
			<div class="col-sm-3 col-md-2">
				Discover and Shop
				<ul class="list-unstyled">
					<li><a href="#">Gift Cards</a></li>
					<li><a href="#">Blog</a></li>
					<li><a href="#">Mobile Apps</a></li>
					<li><a href="#">Wholesale</a></li>
				</ul>
			</div>
			<div class="col-sm-3 col-md-2">
				Get to Know Us
				<ul class="list-unstyled">
					<li><a href="#">About</a></li>
					<li><a href="#">Careers</a></li>
					<li><a href="#">Press</a></li>
					<li><a href="#">Developers</a></li>
				</ul>
			</div>
			<div class="col-sm-3 col-md-2">
				Discover and Shop
				<ul class="list-unstyled">
					<li><a href="#">Gift Cards</a></li>
					<li><a href="#">Blog</a></li>
					<li><a href="#">Mobile Apps</a></li>
					<li><a href="#">Wholesale</a></li>
				</ul>
			</div>
			<div class="col-sm-5 col-md-4 col-lg-3 col-lg-offset-1">
				<div class="margin-medium visible-sm"></div>
				<h4>Join Newsletter</h4>
				<div class="margin-small"></div>
				<!-- <form> -->
					<div class="input-group">
						<label for="Email" class="sr-only">Email address</label>
						<input id="subscribe-mob-box" type="email" class="form-control input-sm" placeholder="your@email.com">
						<span class="input-group-btn"><button id="subscribe-mob-submit" type="submit" class="btn btn-sm btn-primary">SUBSCRIBE</button></span>
					</div>
				<!-- </form> -->
				<div class="margin-small"></div>
				<div class="icons-row">
					<a href="https://twitter.com/kindoff_co" target="_blank"><span class="i-twitter"></span></a>
					<a href="https://www.facebook.com/kindoff.co" target="_blank"><span class="i-facebook"></span></a>
					<a href="http://instagram.com/kindoff.co" target="_blank"><span class="i-instagram"></span></a>
				</div>
				<div class="margin-tiny"></div>
				  &copy; 2015 Kindoff. Ltd
			</div>
		</div>
	</div>
	<div class="margin-medium"></div>
</footer>
<div class="visible-xs small text-center text-muted">
	&copy; 2015 Kindoff. Ltd
	<div class="margin-medium"></div>
</div>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>