<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html xml:lang="en" lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Kindoff - A community of Smart Buying and Selling by Moms Worldwide</title>
	<meta name="description" content="Kindoff — A community of Smart Buying and Selling by Moms Worldwide">
	<meta name="keywords" content="Kindoff, baby, kids, children, e-commerce, goods, items">
	<meta name="robots" content="INDEX,FOLLOW">
	<link rel="icon" href="favicon.ico" type="image/x-icon">
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
	<meta property="og:site_name" content="Kindoff">
	<meta property="og:description" content="Kindoff — A community of Smart Buying and Selling by Moms Worldwide">
	<meta property="og:type" content="website">
	<meta property="og:url" content="http://Kindoff.co/">
	<meta property="og:image" content="http://Kindoff.co/img/opengraph/logo.png">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="mobile-web-app-capable" content="yes">
	<link rel="stylesheet" type="text/css" href="css/base.css">
	<link rel="stylesheet" type="text/css" href="css/xcharts.min.css">
	<script src="js/jquery-2.1.1.min.js"></script>
	<script src="js/modernizr.js"></script>
	<script src="js/retina.js"></script>
	<script src="js/custom.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/xcharts.min.js"></script>
	<script src="js/d3.min.js"></script>
	<script src="js/portalutils.js"></script>
	
	<script type="text/javascript">
	
	$(function(){
		$('#subscribe-mob-submit').click( function() {
			if(validateEmail($('#subscribe-mob-box').val())){
				$.ajax({
	        		url: 'subscribe',
	       			 type: 'post',
	        		contentType: "application/json",
	        		dataType: 'json',
	        		data: JSON.stringify({'subscriberEmail' : $('#subscribe-mob-box').val()}),
	        		success: function(data) {
	            		alert(data.message);     
	            		$('#subscribe-mob-box').val('');
	        		}
	    		});
			}    		
		});
		
		$('#subscribe-des-submit').click( function() {
			if(validateEmail($('#subscribe-des-box').val())){
				$.ajax({
	        		url: 'subscribe',
	       			 type: 'post',
	        		contentType: "application/json",
	        		dataType: 'json',
	        		data: JSON.stringify({'subscriberEmail' : $('#subscribe-des-box').val()}),
	        		success: function(data) {
	            		alert(data.message);  
	            		$('#subscribe-des-box').val('');
	        		}
	    		});
			}else{
				
			}
		});		
	});
	
	</script>
</head>
<body>
<div class="mobile-menu">
	<a href="#" id="mobile-menu-close"><span class="i-close-highlight"></span> <small class="text-primary">CLOSE</small></a>
	<div class="margin-small"></div>
	<div class="list-group">
	  <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Diapering</a>
	  <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>On the Go</a>
	  <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Activities & Play</a>
	  <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Baby Care</a>
	  <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Nursery</a>
	  <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Feeding</a>
	  <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>For Mom</a>
	  <a href="#" class="list-group-item"><span class="i-arrow-right pull-right"></span>Health & Safety</a>
	</div>
	<button class="btn btn-md btn-default btn-wide"><span class="i-bubble-inverted"></span> <span class="hidden-sm">Parent’s</span> Community</button>
	<div class="margin-tiny"></div>
	<button class="btn btn-md btn-default btn-wide"><span class="i-add-inverted"></span> Trade Your Stuff</button>
	<div class="margin-medium"></div>
	Join the Community
	<ul class="list-unstyled">
		<li><a href="#">Teams</a></li>
		<li><a href="#">Forums</a></li>
		<li><a href="#">Upcoming Events</a></li>
		<li><a href="#">Affiliates</a></li>
	</ul>
	<div class="margin-medium"></div>
	Discover and Shop
	<ul class="list-unstyled">
		<li><a href="#">Gift Cards</a></li>
		<li><a href="#">Blog</a></li>
		<li><a href="#">Mobile Apps</a></li>
		<li><a href="#">Wholesale</a></li>
	</ul>
	<div class="margin-medium"></div>
	Get to Know Us
	<ul class="list-unstyled">
		<li><a href="#">About</a></li>
		<li><a href="#">Careers</a></li>
		<li><a href="#">Press</a></li>
		<li><a href="#">Developers</a></li>
	</ul>
	<div class="margin-medium"></div>
	Discover and Shop
	<ul class="list-unstyled">
		<li><a href="#">Gift Cards</a></li>
		<li><a href="#">Blog</a></li>
		<li><a href="#">Mobile Apps</a></li>
		<li><a href="#">Wholesale</a></li>
	</ul>
	<div class="margin-small"></div>
	<hr>
	<div class="margin-small"></div>
	<h4>Join Newsletter</h4>
	<div class="margin-small"></div>
	<!-- <form> -->
		<div class="input-group">
			<label for="Email" class="sr-only">Email address</label>
			<input id="subscribe-mob-box" type="email" class="form-control input-sm" placeholder="your@email.com">
			<span class="input-group-btn"><button id="subscribe-mob-submit" type="submit" class="btn btn-sm btn-primary">SUBSCRIBE</button></span>
		</div>
	<!-- </form> -->
	<div class="margin-small"></div>
	<div class="icons-row">
		<a href="https://twitter.com/kindoff_co" target="_blank"><span class="i-twitter"></span></a>
		<a href="https://www.facebook.com/kindoff.co" target="_blank"><span class="i-facebook"></span></a>
		<a href="http://instagram.com/kindoff.co" target="_blank"><span class="i-instagram"></span></a>
	</div>
</div>
<nav class="navbar navbar-default">
	<div class="container">
		<div class="row">
			<div class="col-xs-5 visible-xs header-nav">
				<ul>
					<li><button class="btn btn-link header-mobile-menu" id="mobile-menu-open"><span class="i-menu"></span></button></li>
					<li>
						<button class="btn btn-link header-item-switcher"><span class="i-search"></span></button>
						<div class="header-item-content">
							<div class="input-group">
								<input type="text" class="form-control" placeholder="Search goods..." aria-label="search-global">
								<div class="input-group-btn">
									<button type="button" class="btn btn-transparent dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
										in all categories <span class="i-arrow-down"></span>
										<span class="sr-only">Toggle Dropdown</span>
									</button>
									<ul class="dropdown-menu dropdown-menu-right" role="menu">
										<li><a href="#">Diapering</a></li>
										<li><a href="#">On the Go</a></li>
										<li><a href="#">Activities & Play</a></li>
										<li><a href="#">Baby Care</a></li>
										<li><a href="#">Nursery</a></li>
										<li><a href="#">Feeding</a></li>
										<li><a href="#">For Mom</a></li>
										<li><a href="#">Health & Safety</a></li>
									</ul>
									<button type="button" class="btn btn-default"><span class="i-search-inverted"></span></button>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<div class="col-xs-2">
				<a href="/KindOff"><span class="i-logo-sm hidden-xs"></span></a>
				<div class="text-center visible-xs"><a href="/KindOff"><span class="i-logo-xs"></span></a></div>
			</div>
			<div class="col-sm-6 col-lg-5 hidden-xs">
				<div class="input-group">
					<input type="text" class="form-control" placeholder="Search goods..." aria-label="search-global">
					<div class="input-group-btn">
						<button type="button" class="btn btn-transparent dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
							in all categories <span class="i-arrow-down"></span>
							<span class="sr-only">Toggle Dropdown</span>
						</button>
						<ul class="dropdown-menu dropdown-menu-right" role="menu">
							<li><a href="#">Diapering</a></li>
							<li><a href="#">On the Go</a></li>
							<li><a href="#">Activities & Play</a></li>
							<li><a href="#">Baby Care</a></li>
							<li><a href="#">Nursery</a></li>
							<li><a href="#">Feeding</a></li>
							<li><a href="#">For Mom</a></li>
							<li><a href="#">Health & Safety</a></li>
						</ul>
						<button type="button" class="btn btn-default"><span class="i-search-inverted"></span></button>
					</div>
				</div>
			</div>
			<div class="col-xs-5 col-sm-4 col-lg-5 header-nav">
				<ul class="pull-right">
					<li class="header-cart">
						<button class="btn btn-link header-item-switcher"><span class="i-cart"></span></button>
						<div class="header-item-content">
							<div class="carousel-cart" style="width:392px">
							    <div class="item">
									<div class="row">
										<div class="col-xs-6">
											<div class="product-list-item inverted">
												<a href="/big" class="product-list-description">
													<img src="img/square-placeholder.png" style="background-image:url(http://lorempixel.com/472/472/fashion/4);" class="img-responsive" alt="">
													<span><b>Disrupt hella cray Thundercats meditation chambray slow cray Thundercats meditation chambray slow</b></span>
												</a>
												<div class="product-list-seller">
													<a href="#" class="user-rating">
														<span class="i-rating-inverted"></span> 6.9
													</a>
													<a href="#" class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/2" alt=""></a>
												</div>
												<div class="product-list-price"><s>$12</s> $7</div>
											</div>
										</div>
										<div class="col-xs-6">
											<div class="product-list-item inverted">
												<a href="/big" class="product-list-description">
													<img src="img/square-placeholder.png" style="background-image:url(http://lorempixel.com/472/472/fashion/5);" class="img-responsive" alt="">
													<span><b>Blue Bottle meggings tofu mustache. Shoreditch wolf meditation chambray slow</b></span>
												</a>
												<div class="product-list-seller">
													<a href="#" class="user-rating">
														<span class="i-rating-inverted"></span> 6.9
													</a>
													<a href="#" class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/3" alt=""></a>
												</div>
												<div class="product-list-price"><s>$12</s> $7</div>
											</div>
										</div>
									</div>
								</div>
							    <div class="item">
									<div class="row">
										<div class="col-xs-6">
											<div class="product-list-item inverted">
												<a href="/big" class="product-list-description">
													<img src="img/square-placeholder.png" style="background-image:url(http://lorempixel.com/472/472/fashion/4);" class="img-responsive" alt="">
													<span><b>Disrupt hella cray Thundercats meditation chambray slow cray Thundercats meditation chambray slow</b></span>
												</a>
												<div class="product-list-seller">
													<a href="#" class="user-rating">
														<span class="i-rating-inverted"></span> 6.9
													</a>
													<a href="#" class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/2" alt=""></a>
												</div>
												<div class="product-list-price"><s>$12</s> $7</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="submit-area clearfix">
								<button class="btn btn-default inverted pull-right">CHECK OUT</button>
								<div class="margin-tiny"></div>
								3 items, grand-total: $56
							</div>
						</div>
					</li>
					<li class="header-profile">
						<button class="btn btn-link header-item-switcher">
							<span class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/1" alt=""></span>
							<small class="hidden-xs">
							<security:authorize access="isAuthenticated()">
                                <security:authentication property="principal.portalUser.firstName" />&nbsp;
                                <security:authentication property="principal.portalUser.lastName" />
							</security:authorize>
							</small>
						</button>
						<div class="header-item-content list-group">
							<a href="dashboard" class="list-group-item">View Profile</a>
							<a href="#" class="list-group-item">Edit Profile</a>
							<a href="#" class="list-group-item">Purchase History</a>
							<a href="#" class="list-group-item">Messages <span class="badge">4</span></a>
							<a href="#" class="list-group-item">Responses</a>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</nav>
<div class="container">
	<div class="row">
		<div class="col-sm-5 col-md-4 col-lg-3 col-lg-offset-1 col-sm-push-7 col-md-push-8 col-lg-push-7 item-general">
			<h1>Floral baby girl dress</h1>
			<div class="margin-tiny"></div>
			<table class="table">
				<tr class="small"><td>Sex:</td><td colspan="2">Female</td></tr>
				<tr class="small"><td>Age:</td><td colspan="2">3-5</td></tr>
				<tr class="small"><td>Condition:</td><td colspan="2">Used</td></tr>
				<tr class="small"><td>Delivery:</td><td colspan="2">Pickup, Courier Service</td></tr>
				<tr class="small"><td>Payments:</td><td colspan="2">PayPal</td></tr>
				<tr class="small"><td>Returns:</td><td colspan="2">No return</td></tr>
				<tr class="small"><td>Guarantee:</td><td colspan="2"><a href="#">Kindoff buyer protection</a></td></tr>
				<tr class="item-general-quantity"><td>Quantity:</td><td width="1%"><input type="text" class="form-control" id="exampleInputEmail1" placeholder="1"></td><td>1 available</td></tr>
				<tr>
					<td>
						<div class="item-general-price">
							$32 <small>+ shipping</small>
						</div>
					</td>
					<td colspan="2">
						<button class="btn btn-default btn-lg btn-wide">
							<span class="i-cart-inverted"></span> Add To Cart
						</button>
						<small><a href="#">Add to wishlist</a></small>
					</td>
				</tr>
			</table>
		</div>
		<div class="col-sm-7 col-md-8 col-lg-6 col-lg-offset-1 col-sm-pull-5 col-md-pull-4 col-lg-pull-4 item-media">
			<div class="carousel-item center-block">
			    <div class="item"><img src="http://lorempixel.com/784/784/fashion/1" alt=""></div>
			    <div class="item"><img src="http://lorempixel.com/784/784/fashion/2" alt=""></div>
			    <div class="item"><img src="http://lorempixel.com/784/784/fashion/3" alt=""></div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-8 col-lg-6 col-lg-offset-1 item-description">
			<hr><div class="margin-medium"></div>
			<p>Meditation American Apparel yr McSweeney's Thundercats, chia Odd Future Pinterest keytar artisan flannel. Thundercats asymmetrical craft beer, keffiyeh tousled cornhole lo-fi biodiesel selvage street art gentrify tilde. Thundercats fixie church-key selvage, gastropub flexitarian distillery lomo pop-up swag.</p>
			<p>Odd Future master cleanse Blue Bottle, YOLO squid raw denim Echo Park cliche banjo umami direct trade art party. Salvia keffiyeh plaid, actually put a bird on it Schlitz Intelligentsia asymmetrical health goth try-hard seitan fingerstache Etsy 3 wolf moon. 8-bit Brooklyn jean shorts next level Portland, deep v keffiyeh tofu hashtag church-key readymade chambray put a bird on it.</p>
			<p>Wolf kitsch fap, Helvetica VHS you probably haven't heard of them letterpress quinoa whatever messenger bag.</p>
			<div class="margin-small"></div>
			<div class="hidden-xs hidden-sm">
				<hr>
				<div class="margin-medium"></div>
				<h4><a href="#">
					<security:authorize access="isAuthenticated()">
                        <security:authentication property="principal.portalUser.firstName" />&nbsp;
                        <security:authentication property="principal.portalUser.lastName" />
					</security:authorize>
				</a></h4>
				<div class="margin-small"></div>
				<div class="row">
					<div class="col-xs-6 col-sm-4 col-md-4">
						<div class="product-list-item">
							<a href="/big" class="product-list-description">
								<img src="img/square-placeholder.png" style="background-image:url(http://lorempixel.com/472/472/fashion/4);" class="img-responsive" alt="">
								<span><b>Disrupt hella cray Thundercats meditation chambray slow cray Thundercats meditation chambray slow</b></span>
							</a>
							<div class="product-list-price"><s>$12</s> $7</div>
						</div>
					</div>
					<div class="col-xs-6 col-sm-4 col-md-4">
						<div class="product-list-item">
							<a href="/big" class="product-list-description">
								<img src="img/square-placeholder.png" style="background-image:url(http://lorempixel.com/472/472/fashion/5);" class="img-responsive" alt="">
								<span><b>Blue Bottle meggings tofu mustache. Shoreditch wolf meditation chambray slow</b></span>
							</a>
							<div class="product-list-price"><s>$12</s> $7</div>
						</div>
					</div>
					<div class="col-xs-6 col-sm-4 col-md-4 hidden-xs">
						<div class="product-list-item">
							<a href="/big" class="product-list-description">
								<img src="img/square-placeholder.png" style="background-image:url(http://lorempixel.com/472/472/fashion/6);" class="img-responsive" alt="">
								<span><b>Messenger bag paleo mustache Pinterest. Schlitz meditation chambray slow</b></span>
							</a>
							<div class="product-list-price"><s>$12</s> $7</div>
						</div>
					</div>
				</div>
				<div class="margin-medium"></div>
				<h4><a href="#">Similar Items</a></h4>
				<div class="margin-small"></div>
				<div class="row">
					<div class="col-xs-6 col-sm-4 col-md-4">
						<div class="product-list-item">
							<a href="/big" class="product-list-description">
								<img src="img/square-placeholder.png" style="background-image:url(http://lorempixel.com/472/472/fashion/8);" class="img-responsive" alt="">
								<span><b>Disrupt hella cray Thundercats meditation chambray slow cray Thundercats meditation chambray slow</b></span>
							</a>
							<div class="product-list-seller">
								<a href="#" class="user-rating">
									<span class="i-rating-inverted"></span> 6.9
								</a>
								<a href="#" class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/2" alt=""></a>
							</div>
							<div class="product-list-price"><s>$12</s> $7</div>
						</div>
					</div>
					<div class="col-xs-6 col-sm-4 col-md-4">
						<div class="product-list-item">
							<a href="/big" class="product-list-description">
								<img src="img/square-placeholder.png" style="background-image:url(http://lorempixel.com/472/472/fashion/9);" class="img-responsive" alt="">
								<span><b>Blue Bottle meggings tofu mustache. Shoreditch wolf meditation chambray slow</b></span>
							</a>
							<div class="product-list-seller">
								<a href="#" class="user-rating">
									<span class="i-rating-inverted"></span> 6.9
								</a>
								<a href="#" class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/3" alt=""></a>
							</div>
							<div class="product-list-price"><s>$12</s> $7</div>
						</div>
					</div>
					<div class="col-xs-6 col-sm-4 col-md-4 hidden-xs">
						<div class="product-list-item">
							<a href="/big" class="product-list-description">
								<img src="img/square-placeholder.png" style="background-image:url(http://lorempixel.com/472/472/fashion/1);" class="img-responsive" alt="">
								<span><b>Messenger bag paleo mustache Pinterest. Schlitz meditation chambray slow</b></span>
							</a>
							<div class="product-list-seller">
								<a href="#" class="user-rating">
									<span class="i-rating-inverted"></span> 6.9
								</a>
								<a href="#" class="user-pic-sm"><img src="http://lorempixel.com/64/64/people/4" alt=""></a>
							</div>
							<div class="product-list-price"><s>$12</s> $7</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-lg-3 col-lg-offset-1 item-seller">
			<div class="panel panel-default">
				<div class="panel-body text-center">
					<a href="#" class="user-pic-lg"><img src="http://lorempixel.com/200/200/people/2" alt=""></a>
					<h4>
					<security:authorize access="isAuthenticated()">
                        <security:authentication property="principal.portalUser.firstName" />&nbsp;
                        <security:authentication property="principal.portalUser.lastName" />
					</security:authorize>
					</h4>
					<small class="text-muted">seller</small>
				</div>
				<div class="list-group">
					
					<div class="list-group-item"><a href="#" class="user-rating"><span class="i-rating-inverted"></span> 6.9</a> <small class="text-muted">100% positive feedback</small></div>
					<a href="dashboard" class="list-group-item">View profile</a>
					<a href="#" class="list-group-item">Follow</a>
					<a href="#" class="list-group-item">View other items</a>
					<a href="#" class="list-group-item">View forums profile</a>
					<a href="#" class="list-group-item">Ask a question about the item</a>
				</div>
			</div>
			<div class="margin-small"></div>
			<div class="cut-out">
				<h4>Shipping Calculator</h4>
				<div class="margin-medium"></div>
				<form class="form-horizontal small">
					<div class="form-group">
						<label for="inputEmail3" class="col-xs-3 control-label">Country</label>
						<div class="col-xs-9">
							<select class="form-control">
							  <option>Canada</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="inputPassword3" class="col-xs-3 control-label">City</label>
						<div class="col-xs-9">
							<select class="form-control">
							  <option>Vancouver</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="inputPassword3" class="col-xs-3 control-label">Type</label>
						<div class="col-xs-9">
							<select class="form-control">
							  <option>Express</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<button type="submit" class="btn btn-default inverted btn-wide">Get Rates</button>
						</div>
					</div>
				</form>
			</div>
			<div class="margin-large"></div>
			<img src="http://lorempixel.com/400/300/" class="img-responsive center-block" alt="">
		</div>
	</div>
</div>
<div class="margin-large"></div>
<footer class="hidden-xs">
	<div class="margin-medium"></div>
	<div class="container">
		<div class="row">
			<div class="col-sm-3 col-md-2">
				Join the Community
				<ul class="list-unstyled">
					<li><a href="#">Teams</a></li>
					<li><a href="#">Forums</a></li>
					<li><a href="#">Upcoming Events</a></li>
					<li><a href="#">Affiliates</a></li>
				</ul>
			</div>
			<div class="col-sm-3 col-md-2">
				Discover and Shop
				<ul class="list-unstyled">
					<li><a href="#">Gift Cards</a></li>
					<li><a href="#">Blog</a></li>
					<li><a href="#">Mobile Apps</a></li>
					<li><a href="#">Wholesale</a></li>
				</ul>
			</div>
			<div class="col-sm-3 col-md-2">
				Get to Know Us
				<ul class="list-unstyled">
					<li><a href="#">About</a></li>
					<li><a href="#">Careers</a></li>
					<li><a href="#">Press</a></li>
					<li><a href="#">Developers</a></li>
				</ul>
			</div>
			<div class="col-sm-3 col-md-2">
				Discover and Shop
				<ul class="list-unstyled">
					<li><a href="#">Gift Cards</a></li>
					<li><a href="#">Blog</a></li>
					<li><a href="#">Mobile Apps</a></li>
					<li><a href="#">Wholesale</a></li>
				</ul>
			</div>
			<div class="col-sm-5 col-md-4 col-lg-3 col-lg-offset-1">
				<div class="margin-medium visible-sm"></div>
				<h4>Join Newsletter</h4>
				<div class="margin-small"></div>
				<!-- <form> -->
					<div class="input-group">
						<label for="Email" class="sr-only">Email address</label>
						<input id="subscribe-des-box" type="email" class="form-control input-sm" placeholder="your@email.com">
						<span class="input-group-btn"><button id="subscribe-des-submit" type="submit" class="btn btn-sm btn-primary">SUBSCRIBE</button></span>
					</div>
				<!-- </form> -->
				<div class="margin-small"></div>
				<div class="icons-row">
					<a href="https://twitter.com/kindoff_co" target="_blank"><span class="i-twitter"></span></a>
					<a href="https://www.facebook.com/kindoff.co" target="_blank"><span class="i-facebook"></span></a>
					<a href="http://instagram.com/kindoff.co" target="_blank"><span class="i-instagram"></span></a>
				</div>
				<div class="margin-tiny"></div>
					&copy; 2015 Kindoff. Ltd
			</div>
		</div>
	</div>
	<div class="margin-medium"></div>
</footer>
<div class="visible-xs small text-center text-muted">
	&copy; 2015 Kindoff. Ltd
	<div class="margin-medium"></div>
</div>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>