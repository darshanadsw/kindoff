/**
 * Created by darshana on 22/04/15.
 */
$(document).ready(function(){

    // subscription
    $('#subscribe-mob-submit').click(function() {
        if (validateEmail($('#subscribe-mob-box').val())) {
            $.ajax({
                url : 'subscribe',
                type : 'post',
                contentType : "application/json",
                dataType : 'json',
                data : JSON.stringify({
                    'subscriberEmail' : $('#subscribe-mob-box').val()
                }),
                success : function(data) {
                    $("#spinner").hide();
                    alert(data.message);
                    $('#subscribe-mob-box').val('');
                }
            });
        }
    });

    $('#subscribe-des-submit').click(function() {
        if (validateEmail($('#subscribe-des-box').val())) {
            $.ajax({
                url : 'subscribe',
                type : 'post',
                contentType : "application/json",
                dataType : 'json',
                data : JSON.stringify({
                    'subscriberEmail' : $('#subscribe-des-box').val()
                }),
                success : function(data) {
                    $("#spinner").hide();
                    alert(data.message);
                    $('#subscribe-des-box').val('');
                }
            });
        }
    });

});