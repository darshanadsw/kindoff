var resizeTimer;



$(document).ready(function() {
	var p=$('.product-list-description span b');
	var divh=$('.product-list-description span').height();
	while ($(p).outerHeight()>divh) {
	    $(p).text(function (index, text) {
	        return text.replace(/\W*\s(\S)*$/, '…');
	    });
	}



	$('.carousel-cart').owlCarousel({
		margin:24,
		items:1,
		dots:true
	});
	

	$('.carousel-main').owlCarousel({
		items:2,
		autoplay:true,
		autoplayTimeout:5000,
		dots:true,
		loop:true,
		margin:24
	});

	$('.carousel-item').owlCarousel({
		items:1,
		autoplay:true,
		autoplayTimeout:10000,
		dots:true,
		loop:true,
		margin:24,
		center: true
	});
	
	$("#mobile-menu-open").click(function() {
		event.preventDefault();
		$(".mobile-menu").addClass("open");
	});
	
	$("#mobile-menu-close").click(function() {
		event.preventDefault();
		$(".mobile-menu").removeClass("open");
	});

    initApp = function() {
        $(".header-item-switcher").hover(function() {
            $(".header-item-content").removeClass("open");
            $(".header-item-switcher").removeClass("active");
            $(this).addClass("active");
            $(this).parent("li").children(".header-item-content").addClass("open");
        });
        $(".header-item-switcher").on("touchend", function(e) {
            e.preventDefault();
            $(".header-item-content").removeClass("open");
            $(".header-item-switcher").removeClass("active");
            $(this).addClass("active");
            $(this).parent("li").children(".header-item-content").addClass("open");
            return false;
        });
        $(document).click(function(e) {
            var container = $(".header-item-content");
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                container.removeClass("open");
                $(".header-item-switcher").removeClass("active");
            }
        });
    }
    initApp();
});