/**
 * 
 */

function loadKeyValueByType(type, compId) {
	$.ajax({
		type : "GET",
		url : "loadKeyValue/" + type,
		data : {},
		contentType : "application/json",
		dataType : "json",
		success : function(arten) {
			$("#" + compId).empty();
			$("#" + compId).append($("<option></option>").val('').html('Any'));
			$.each(arten, function() {
				$("#" + compId).append(
						$("<option></option>").val(this['key']).html(
								this['value']));
			});
		}
	});
}

function validateEmail(sEmail) {
	var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if (filter.test(sEmail)) {
		return true;
	} else {
		return false;
	}
}